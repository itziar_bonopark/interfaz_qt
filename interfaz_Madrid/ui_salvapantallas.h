/********************************************************************************
** Form generated from reading UI file 'salvapantallas.ui'
**
** Created: Mon Feb 9 10:27:01 2015
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SALVAPANTALLAS_H
#define UI_SALVAPANTALLAS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Salvapantallas
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *layoutVideo;

    void setupUi(QDialog *Salvapantallas)
    {
        if (Salvapantallas->objectName().isEmpty())
            Salvapantallas->setObjectName(QString::fromUtf8("Salvapantallas"));
        Salvapantallas->resize(400, 300);
        gridLayout = new QGridLayout(Salvapantallas);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        layoutVideo = new QVBoxLayout();
        layoutVideo->setSpacing(0);
        layoutVideo->setObjectName(QString::fromUtf8("layoutVideo"));
        layoutVideo->setSizeConstraint(QLayout::SetDefaultConstraint);
        layoutVideo->setContentsMargins(0, -1, -1, -1);

        gridLayout->addLayout(layoutVideo, 0, 0, 1, 1);


        retranslateUi(Salvapantallas);

        QMetaObject::connectSlotsByName(Salvapantallas);
    } // setupUi

    void retranslateUi(QDialog *Salvapantallas)
    {
        Salvapantallas->setWindowTitle(QApplication::translate("Salvapantallas", "Dialog", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Salvapantallas: public Ui_Salvapantallas {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SALVAPANTALLAS_H
