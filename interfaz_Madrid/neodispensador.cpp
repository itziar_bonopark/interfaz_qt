#include "neodispensador.h"

neoDispensador::neoDispensador(QObject *parent) :
    QThread(parent)
{
    this->inicializacion();
    this->setEstado(ESTADO_DISPENSADOR_REPOSO);
    //this->abrirPuerto();
}
void neoDispensador::inicializacion()
{
    this->estadoDefinido=false;
    this->intentoLectura=0;
    this->esperandoLeerTarjeta=false;

    this->esperandoRespuesta=false;

    this->bTO3CardDetected = false;// devolver[11]= false;
    this->bTO2CardDetected = false;// devolver[10]= false;
    this->bTO1CardDetected = false;// devolver[9]= false;
    this->bStackHolderEmpty = false;// devolver[8]= false;
    this->bHolderPreEmpty = false;// devolver[7]= false;
    this->bCardJamming = false;// devolver[6]= false;
    this->bROCardDetected = false;// devolver[5]= false;
    this->bReservation = false;// devolver[4]= false;
    this->bReclaimingError = false;// devolver[3]= false;
    this->bDispensingError = false;// devolver[2]= false;
    this->bReclaiming = false;// devolver[1]= false;
    this->bDispensing = false;// devolver[0]= false;

    this->tarjetas_dispensador=true;
}
int neoDispensador::getEstado()
{
    return this->estado;
}

void neoDispensador::setEstado(int est)
{
    this->estado=est;
}

void neoDispensador::abrirPuerto()
{
    qDebug()<<"iniciando puerto serie del dispensador...";
      this->esperandoRespuesta=false;

      bool ok=true;
      serial_port.Open( this->puerto.toStdString());


      if ( ! serial_port.good() )
      {
          qDebug()<< "Error: Could not open serial port.";
          ok=false;
      }
      //
      // Set the baud rate of the serial port.
      //
      serial_port.SetBaudRate( SerialStreamBuf::BAUD_9600 ) ;
      if ( ! serial_port.good() )
      {
         qDebug()<<"Error: Could not set the baud rate.";
         ok=false;
      }
      //
      // Set the number of data bits.
      //
      serial_port.SetCharSize( SerialStreamBuf::CHAR_SIZE_8 ) ;
      if ( ! serial_port.good() )
      {
          qDebug()<< "Error: Could not set the character size.";
         ok=false;
      }
      //
      // Disable parity.
      //
      serial_port.SetParity( SerialStreamBuf::PARITY_NONE ) ;
      if ( ! serial_port.good() )
      {
          qDebug()<< "Error: Could not disable the parity.";
          ok=false;
      }
      //
      // Set the number of stop bits.
      //
      serial_port.SetNumOfStopBits( 1 ) ;
      if ( ! serial_port.good() )
      {
         qDebug()<< "Error: Could not set the number of stop bits.";
         ok=false;
      }
      //
      // Turn on hardware flow control.
      //
      serial_port.SetFlowControl( SerialStreamBuf::FLOW_CONTROL_NONE ) ;
      if ( ! serial_port.good() )
      {
          qDebug()<< "Error: Could not use hardware flow control.";
          ok=false;
      }
      if(ok)
      {
          qDebug()<<"puerto abierto para el dispensador!";
          this->posible=true;

      }
      else
      {
          this->posible=false;
          qCritical()<<"puerto no abierto dispensador. Se cierra el programa";
          exit(1);// Cuando se cierra el programa se indica que ha sido el exit 1
      }
}
QByteArray neoDispensador::prepararMensajeEnCasoDeDiferente(QByteArray msg)
{
    QByteArray salida;
    int suma=0;
    for(int i=0;i<msg.count();i++)
    {
        if((msg.at(i)==0x02)||
                (msg.at(i)==0x03)||
                (msg.at(i)==0x10))
        {
            salida.append(0x10);
            suma++;
        }
        salida.append(msg.at(i));
    }

    salida[2]=16+suma+4;
    return salida;
}


QByteArray neoDispensador::escribeYespera(QByteArray msg, int ms,bool diferente)
{
    QByteArray salida;
    this->serial_port.write(msg.data(),msg.count());

    QDateTime ahora=QDateTime::currentDateTime();
    QDateTime antes=QDateTime::currentDateTime();
    bool yasecuantos=false;
    bool timeout=false;
    bool ignorando=false;
    while ((!yasecuantos)&&(!timeout))
    {
        ahora=QDateTime::currentDateTime();
        int dif=antes.msecsTo(ahora);
        if(dif>ms)
        {
            timeout=true;
        }
        while( serial_port.rdbuf()->in_avail() > 0  )
        {
            char next_byte;
            char f[1];
            serial_port.get(next_byte);
            f[0]=next_byte;
            if(!diferente)
            {
                this->bufferRecepcion.append(QByteArray(f)[0]);
            }
            else
            {
                if(!ignorando)
                {
                    if(quint8(f[0])!=0x10)
                    {
                        this->bufferRecepcion.append(QByteArray(f)[0]);
                    }
                    else
                    {
                       //qDebug()<<"ignorando "<<quint8(f[0]);
                       ignorando=true;
                    }
                }
                else
                {
                    ignorando=false;
                    this->bufferRecepcion.append(QByteArray(f)[0]);
                }
            }

        }
         if(this->bufferRecepcion.count()>2)
         {
             if(diferente)
             {
                numBytesEsperados=5+quint8(this->bufferRecepcion[3]);
             }
             else
             {
                numBytesEsperados=4+this->bufferRecepcion[2];
             }
             if(this->bufferRecepcion.count()>=this->numBytesEsperados)
             {
                yasecuantos=true;
             }
         }
    }
    if(!timeout)
    {
        if(this->bufferRecepcion.toHex()=="")
        {
            //qDebug()<<"eing?";
        }
        salida=this->bufferRecepcion;
        this->listaComandos.removeFirst();
        this->bufferRecepcion.clear();
    }

    else
    {
        salida=this->bufferRecepcion;
        //qDebug()<<"ha sido por timeout... llegado:"<<salida.toHex();
        this->bufferRecepcion.clear();
    }
    msleep(50);
    return salida;
}


void neoDispensador::run()
{
    while(posible)
    {
        msleep(150);
        if(this->listaComandos.count()>0)
        {
            if(!this->esperandoRespuesta)
            {
                this->esperandoRespuesta=true;
                int cmd=this->listaComandos.first().comando;
                QByteArray argumentos=this->listaComandos.first().argumentos;

                switch(cmd)
                {
                    case COMANDO_DISPENSADOR_REQUEST:
                    {
                        this->request();
                        this->esperandoRespuesta=false;
                        break;
                    }
                                             // 0;
                    case COMANDO_DISPENSADOR_anticoll    :
                    {
                        this->anticoll();
                        this->esperandoRespuesta=false;
                        break;
                    }                          // 1 ;//);
                    case COMANDO_DISPENSADOR_seleccionar  :
                    {
                        this->seleccionar(argumentos);
                    this->esperandoRespuesta=false;
                        break;
                    }                         // 2 ;//);
                case COMANDO_DISPENSADOR_VERIFY:
                {
                    QByteArray key=argumentos.mid(0,6);
                    quint8 bloque=argumentos.at(6);
                    quint8 sector=argumentos.at(7);

                    this->verify(key,bloque,sector);
                    this->esperandoRespuesta=false;
                    break;

                }
                    case COMANDO_DISPENSADOR_agregar   :
                    {
                        break;
                    }                            // 3 ;//int sector, int bloque, int operacion);
                    case COMANDO_DISPENSADOR_transfer   :
                    {
                        break;
                    }                           // 4 ;//);
                    case COMANDO_DISPENSADOR_irakurri  :
                    {
                        int bloque=argumentos[0];
                        int sector=argumentos[1];
                        this->leer(bloque,sector);
                        this->esperandoRespuesta=false;

                        break;
                    }                            // 5 ;//int bloque, int sector);
                    case COMANDO_DISPENSADOR_loadkey  :
                    {
                        this->loadkey(argumentos);
                        this->esperandoRespuesta=false;
                        break;
                    }                             // 6 ;//QList<QString> codigo);

                    case COMANDO_DISPENSADOR_authenticate :
                    {
                        int bloque=argumentos[0];
                        int sector=argumentos[1];
                        this->authenticate(sector,bloque);
                        this->esperandoRespuesta=false;
                        break;
                    }                         // 7 ;//);
                    case COMANDO_DISPENSADOR_decrease   :
                    {
                        break;
                    }                           // 8 ;//int bloque,int sector, int operacion);
                    case COMANDO_DISPENSADOR_idatzi     :
                    {
                        int bloque=argumentos[0];
                        int sector=argumentos[1];
                        QByteArray datos=argumentos.mid(2,16);
                        this->escribe(sector,bloque,datos);
                        this->esperandoRespuesta=false;
                        break;
                    }                           // 9 ;//int sector, int bloque, QList<QString> datos);
                    case COMANDO_DISPENSADOR_semi_dispensing:
                    {
                        this->semidispensing();
                        this->esperandoRespuesta=false;
                        break;
                    }                       // 10 ;//); //pasa a la posicion de lectura de tarjeta
                    case COMANDO_DISPENSADOR_semi_popping :
                    {
                        this->semipoping();
                        this->esperandoRespuesta=false;
                        break;
                    }                         // 11 ;//);// dispensa la tarjeta
                    case COMANDO_DISPENSADOR_fullout :
                    {
                        break;
                    }                              // 12 ;//);
                    case COMANDO_DISPENSADOR_reclaim :
                    {
                        this->reclaim();
                        this->esperandoRespuesta=false;
                        break;
                    }                              // 13 ;//);
                    case COMANDO_DISPENSADOR_getStatus :
                    {
                        this->getStatus();
                        this->esperandoRespuesta=false;
                        break;
                    }                            // 14 ;//);
                    case COMANDO_DISPENSADOR_comprobarSiHayTarjetaSinHaberlaPedido :
                    {
                        break;
                    }// 15 ;//);
                    case COMANDO_DISPENSADOR_setRF:
                    {
                        break;
                    }                                 // 16 ;//bool on);

                    case COMANDO_DISPENSADOR_reset:
                    {
                    this->resetDispensador();
                        break;
                    }                                 // 17 ;//);
                    case COMANDO_DISPENSADOR_setBuzzer:
                    {
                        break;
                    }


                }
                this->esperandoRespuesta=false;
            }
        }
        else
        {
            this->getStatus();
        }

    }
}
void neoDispensador::agregarALaLista(STR_MENSAJE msg)
{
    this->listaComandos.append(msg);
}
quint8 neoDispensador::checksum(QByteArray buf)
{
    // Rutina que calcula el Checksum
   // BYTE calCKSUM(BYTE *buf, BYTE len){
    int len=buf.count();
    BYTE i;
        bool esControl=false;
        quint32 CKSUM = 0;
        for (i=1; i<len; i++){
            if(!esControl)
            {
                if(quint8(buf[i])==0x10)
                {
                    esControl=true;
                }
                else
                {
                    CKSUM += quint8(buf[i]);
                }
            }
            else
            {
                CKSUM += quint8(buf[i]);
                esControl=false;
            }
        }
            QByteArray arr;
            arr.append(CKSUM);
            return (L(CKSUM));
    }


quint8 neoDispensador::getXor(QByteArray mensaje,int len)
{
    quint8 resultado;
    resultado=mensaje[1];
    for(int i=2;i<len;i++)
    {
        resultado=resultado^mensaje[i];
    }

    return resultado;
}
void neoDispensador::limpiarLista()
{
    this->listaComandos.clear();
}
void neoDispensador::setEstadoDefinido(bool v)
{
    this->estadoDefinido=v;
}

bool neoDispensador::getEstadoDefinido()
{
    return this->estadoDefinido;
}

void neoDispensador::getStatus()
{
    QByteArray devolver;
    bool timeout=false;
    QByteArray mensaje;
    mensaje.resize(6);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=0x02;
    mensaje[3]=0x2f;
    mensaje[4]=0x45;

    quint8 bcc=this->getXor(mensaje,5);
    mensaje[5]=bcc;

   this->serial_port.write(mensaje.data(),mensaje.count());

    bool yasecuantos=false;
    QDateTime ahora=QDateTime::currentDateTime();
    QDateTime antes=QDateTime::currentDateTime();
    while ((!yasecuantos)&&(!timeout))
    {
        msleep(20);
        ahora=QDateTime::currentDateTime();
        int ms=antes.msecsTo(ahora);
        if(ms>500)
        {
            timeout=true;
            break;
        }
        while( serial_port.rdbuf()->in_avail() > 0  )
        {
            char next_byte;//=new char[1];
            char* f=new char[1];
            serial_port.get(next_byte);
            f[0]=next_byte;
            this->bufferRecepcion.append(QByteArray(f)[0]);
        }
         if(this->bufferRecepcion.count()>2)
         {

             numBytesEsperados=4+this->bufferRecepcion[2];
             if(this->bufferRecepcion.count()>=this->numBytesEsperados)
             {
                 yasecuantos=true;
             }
         }
    }
   if(!timeout)
   {
       fallosConsecutivos=0;
       int x=this->getXor(this->bufferRecepcion,this->bufferRecepcion.count()-1);
       if(x==this->bufferRecepcion.at(this->bufferRecepcion.count()-1))
       {
           for(int i=0;i<3;i++)
           {
               QByteArray temp;
               temp.append(intToBits(this->bufferRecepcion[4+i]));
               temp.remove(0,4);

               devolver.append(temp);
           }
           if(devolver.count()>10)
           {
               bTO3CardDetected = devolver[11];
               bTO2CardDetected = devolver[10];
               bTO1CardDetected = devolver[9];
               bStackHolderEmpty = devolver[8];
               bHolderPreEmpty = devolver[7];
               bCardJamming = devolver[6];
               bROCardDetected = devolver[5];
               bReservation = devolver[4];
               bReclaimingError = devolver[3];
               bDispensingError = devolver[2];
               bReclaiming = devolver[1];
               bDispensing = devolver[0];
               this->estadoDefinido=true;
           }


       }
       else
       {
           qDebug()<<"Xor no coincide...";
       }

       this->bufferRecepcion.clear();
   }

   else
   {
       fallosConsecutivos++;
       if(fallosConsecutivos==5)
       {
          // this->posible=false;
       }
       qDebug()<<"ha sido por timeout... ESTADO";
       this->bufferRecepcion.clear();
   }
}
void neoDispensador::resetRFID()
{
    this->rfid.clear();
}

void neoDispensador::request()
{
    qDebug()<<"REQUEST";

    QByteArray mensaje;

    mensaje.resize(8);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=0x00;
    mensaje[3]=0x04;
    mensaje[4]=0x46;
    mensaje[5]=0x52;
    mensaje[6]=0x9c;
    mensaje[7]=0x03;

    QByteArray salida=this->escribeYespera(mensaje,200,true);

   // qDebug()<<salida.toHex();

}
void neoDispensador::anticoll()
{
    qDebug()<<"ANTICOL";
    //this->timeout=false;
    QByteArray mensaje;
    mensaje.resize(8);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=0x00;
    mensaje[3]=0x04;
    mensaje[4]=0x47;
    mensaje[5]=0x04;
    mensaje[6]=0x4f;
    mensaje[7]=0x03;

    QByteArray rsp=this->escribeYespera(mensaje,200,true);
    if(rsp.count()==12)
    {
        QByteArray neo=rsp.mid(6,4);
        this->rfid=neo;
        //qDebug()<<"La tarjeta:"<<rfid.toHex();

    }
    else
    {
        this->idrfid.clear();
    }


}
QByteArray neoDispensador::prepararMensajeEnCasoDeDiferente1(QByteArray msg)
{
    QByteArray salida;
    int suma=0;
    for(int i=0;i<msg.count();i++)
    {
        if((msg.at(i)==0x02)||
                (msg.at(i)==0x03)||
                (msg.at(i)==0x10))
        {
            salida.append(0x10);
            suma++;
        }
        salida.append(msg.at(i));
    }

  return salida;
}
void neoDispensador::seleccionar(QByteArray rfid)
{
    qDebug()<<"SELECCIONAR:";

    QByteArray mensaje;
    quint8 zero=0;
    mensaje.append(zero);
    mensaje.append(zero);
    mensaje.append(7);
    mensaje.append(0x48);
    mensaje.append(this->getRfid());


    QByteArray neomensaje;
    neomensaje.append(0x02);
    neomensaje.append(this->prepararMensajeEnCasoDeDiferente1(mensaje));
    neomensaje.append(checksum(neomensaje));
    neomensaje.append(0x03);

    QByteArray res= this->escribeYespera(neomensaje,200,true);



}

void neoDispensador::agregar(int sector,int bloque,int operacion)
{
   QByteArray mensaje;
   mensaje.resize(11);
   mensaje[0]=0x02;
   mensaje[1]=0x00;
   mensaje[2]=0x07;
   mensaje[3]=0x3a;
   mensaje[4]=0xc1;

   int noBloque=4*(sector)+bloque;

   mensaje[5]=noBloque;

   QByteArray num;

   num.append(operacion);

   if(num.count()<4)
   {
       int znt=num.count();
       for(int i=0;i<4-znt;i++)
       {
           QByteArray tmp;
           tmp.resize(1);
           tmp[0]=0;
           num.prepend(tmp);
       }
   }
   if(num.count()>4)
   {
       num.truncate(3);
   }

   for(int i=0;i<4;i++)mensaje[6+i]=num[i];

   quint8 bcc=this->getXor(mensaje,10);
   mensaje[10]=bcc;

   this->escribeYespera(mensaje);


}

void neoDispensador::transfer()
{
   QByteArray mensaje;
   mensaje.resize(5);
   mensaje[0]=0x02;
   mensaje[1]=0x00;
   mensaje[2]=quint8(0x02);
   mensaje[3]=0x3b;
   mensaje[4]=0x11;

   quint8 bcc=this->getXor(mensaje,5);
   mensaje[5]=bcc;

   this->escribeYespera(mensaje);


}
void neoDispensador::semi_dispensing()
{
        qDebug()<<"SEMIDISPENSAR:";
    QByteArray mensaje;
    mensaje.resize(5);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=quint8(0x02);
    mensaje[3]=0x2F;
    mensaje[4]=0x40;

    quint8 bcc=this->getXor(mensaje,5);
    mensaje[5]=bcc;
    this->escribeYespera(mensaje);

}

void neoDispensador::semi_popping()
{
        qDebug()<<"SEMIPOPING:";
    QByteArray mensaje;
    mensaje.resize(5);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=quint8(0x02);
    mensaje[3]=0x2F;
    mensaje[4]=0x43;

    quint8 bcc=this->getXor(mensaje,5);
    mensaje[5]=bcc;
    this->escribeYespera(mensaje);


}
void neoDispensador::fullout()
{
    QByteArray mensaje;
    mensaje.resize(5);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=quint8(0x02);
    mensaje[3]=0x2F;
    mensaje[4]=0x41;

    quint8 bcc=this->getXor(mensaje,5);
    mensaje[5]=bcc;
    this->escribeYespera(mensaje);
}

void neoDispensador::reclaim()
{

    qDebug()<<"RECLAMANDO LA TARJETA!!";
    QByteArray mensaje;
    mensaje.resize(5);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=quint8(0x02);
    mensaje[3]=0x2F;
    mensaje[4]=0x42;

    quint8 bcc=this->getXor(mensaje,5);
    mensaje[5]=bcc;

    this->escribeYespera(mensaje,2500);




}
QByteArray neoDispensador::leer(int bloque,int sector)
{
        qDebug()<<"LEER:";
    QByteArray mensaje;
    mensaje.append(0x02);
    quint8 zero=0;
    mensaje.append(zero);
    mensaje.append(zero);
    mensaje.append(0x04);
    mensaje.append(0x4b);

    int noBloque=4*sector+bloque;

    mensaje.append(noBloque);

    quint8 creoBCC=this->checksum(mensaje);
    mensaje.append(quint8(creoBCC));
    mensaje.append(0x03);

    this->numBytesEsperados=21;
    this->respuesta=this->escribeYespera(mensaje,400,true);
   // qDebug()<<"La respuesta de la lectura"<<this->respuesta.toHex();
    emit this->sgnLecturaFinalizadaDelBloqueYsector(this->respuesta);
    QByteArray tarjeta=this->respuesta.mid(6,7);
   return this->respuesta;

}
void neoDispensador::verify(QByteArray codigo,int bloque,int sector)
{
        qDebug()<<"VERIFICAR:";
     QByteArray mensaje;

     int noBloque=4*sector+bloque;

     quint8 zero=0;
     mensaje.append(0x02);
     mensaje.append(zero);
     mensaje.append(zero);
     mensaje.append(0x0b);
     mensaje.append(0x4a);
     mensaje.append(0x60);
     mensaje.append(noBloque);
     mensaje.append(codigo);
     mensaje.append(this->checksum(mensaje));//,0,mensaje.count()));
     mensaje.append(0x03);

     QByteArray sal=this->escribeYespera(mensaje,200,true);
    // qDebug()<<sal.toHex();


}

void neoDispensador::loadkey(QByteArray codigo)
{
    QByteArray mensaje;
    mensaje.resize(11);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[1]=0x00;
    mensaje[1]=0x0b;
    mensaje[1]=0x4a;
    mensaje[1]=0x60;
    mensaje[2]=quint8(0x7);
    mensaje[3]=0x35;
    mensaje[4]=quint8(codigo[0]);
    mensaje[5]=quint8(codigo[1]);
    mensaje[6]=quint8(codigo[2]);
    mensaje[7]=quint8(codigo[3]);
    mensaje[8]=quint8(codigo[4]);
    mensaje[9]=quint8(codigo[5]);

    quint8 creoBCC=this->getXor(mensaje,10);
    mensaje[10]=quint8(creoBCC);
    this->numBytesEsperados=5;

    QByteArray sali=this->escribeYespera(mensaje);
    //qDebug()<<"La respuesta al load key:"<<sali.toHex();

   // qDebug()<<"al fin:"<<this->respuesta.toHex();

}

void neoDispensador::authenticate(int sector,int bloque)
{
    QByteArray mensaje;
    mensaje.resize(11);
    for(int i=0;i<11;i++)mensaje[i]=0;
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=quint8(0x7);
    mensaje[3]=0x37;

    mensaje[4]=0x60;

    int noBloque=4*sector+bloque;

    mensaje[5]=noBloque;



    for(int i=0;i<4;i++)mensaje[6+i]=this->rfid[i];


    quint8 creoBCC=this->getXor(mensaje,10);
    mensaje[10]=quint8(creoBCC);
    this->numBytesEsperados=5;
    //qDebug()<<"escritura de auth:"<<mensaje.toHex();
    this->escribeYespera(mensaje);
    // qDebug()<<"La respuesta de la autenticacion"<<this->respuesta.toHex();


}


void neoDispensador::decrease(int bloque,int sector,int operacion)
{
    QByteArray mensaje;
    mensaje.resize(11);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=0x07;
    mensaje[3]=0x3a;
    mensaje[4]=0xc0;
    int noBloque=4*sector+bloque;

    mensaje[5]=noBloque;
    QByteArray num;
    num.append(operacion);
    if(num.count()<4)
    {
        int znt=num.count();
        for(int i=0;i<4-znt;i++)
        {
            QByteArray tmp;
            tmp.resize(1);
            tmp[0]=0;
            num.prepend(tmp);
        }
    }
    if(num.count()>4)
    {
        num.truncate(3);
    }

    for(int i=0;i<4;i++)mensaje[6+i]=num[i];

    quint8 bcc=this->getXor(mensaje,10);
    mensaje[10]=bcc;

    this->escribeYespera(mensaje);


}

void neoDispensador::escribe(int sector,int bloque,QByteArray datos)
{
        qDebug()<<"ESCRIBIR:";
    QByteArray mensaje;
    quint8 zero=0;
    mensaje.append(0xFF);
    mensaje.append(0xFF);
    mensaje.append(0x14);
    mensaje.append(0x4c);



    int noBloque=4*sector+bloque;

    mensaje.append(noBloque);


    for(int i=0;i<16;i++)
    {
        mensaje.append(datos[i]);
    }

    QByteArray neomensaje;
    neomensaje.append(0x02);
    neomensaje.append(this->prepararMensajeEnCasoDeDiferente(mensaje));
    neomensaje.append(checksum(neomensaje));
    neomensaje.append(0x03);


   QByteArray res= this->escribeYespera(neomensaje,200,true);
   qDebug()<<res.toHex();



}
void neoDispensador::semipoping()
{
    QByteArray mensaje;
    mensaje.resize(5);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=quint8(0x02);
    mensaje[3]=0x2F;
    mensaje[4]=0x43;

    quint8 bcc=this->getXor(mensaje,5);
    mensaje[5]=bcc;
    this->escribeYespera(mensaje);
}
void neoDispensador::semidispensing()
{
    QByteArray mensaje;
    mensaje.resize(5);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=quint8(0x02);
    mensaje[3]=0x2F;
    mensaje[4]=0x40;

    quint8 bcc=this->getXor(mensaje,5);
    mensaje[5]=bcc;
    this->escribeYespera(mensaje);
}
void neoDispensador::resetDispensador()
{
    QByteArray mensaje;
    mensaje.resize(8);
    mensaje[0]=0x02;
    mensaje[1]=0x00;
    mensaje[2]=quint8(0x00);
    mensaje[3]=0x04;
    mensaje[4]=0x53;
    mensaje[5]=quint8(0x27);
    mensaje[6]=0x7f;
    mensaje[7]=0x03;


    this->escribeYespera(mensaje);
}

QByteArray neoDispensador::getRfid()
{
    return this->rfid;
}


QByteArray neoDispensador::intToBits(quint8 entero)
{
   QByteArray salida;
   int ini=entero;
   while(ini>0)
   {
       int ret=ini%2;
       salida.prepend(quint8(ret));
       ini=ini/2;
   }
   for(int i=salida.count();i<8;i++)
   {
       quint8 cero=0x00;
       salida.prepend(cero);
   }


   return salida;
}


bool neoDispensador::getbTO3CardDetected ()
{
    return this->bTO3CardDetected;
}

bool  neoDispensador::getbTO2CardDetected ()
{
    return this->bTO2CardDetected;
}
bool  neoDispensador::getbTO1CardDetected ()
{
    return this->bTO1CardDetected;
}
bool  neoDispensador::getbStackHolderEmpty ()
{
    return this->bStackHolderEmpty;
}
bool  neoDispensador::getbHolderPreEmpty ()
{
    return this->bHolderPreEmpty;
}
bool  neoDispensador::getbCardJamming ()
{
    return this->bCardJamming;
}
bool  neoDispensador::getbROCardDetected ()
{
    return this->bROCardDetected;
}
bool  neoDispensador::getbReservation ()
{
    return this->bReservation;
}
bool  neoDispensador::getbReclaimingError ()
{
    return this->bReclaimingError;
}
bool  neoDispensador::getbDispensingError ()
{
    return this->bDispensingError;
}
bool  neoDispensador::getbReclaiming ()
{
    return this->bReclaiming;
}
bool  neoDispensador::getbDispensing ()
{
    return this->bDispensing;
}


void neoDispensador::esperar(int ms)
{

    QEventLoop loop;
    QTimer timer;

    timer.setSingleShot(true);
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    //connect(manager, SIGNAL(finished()), &loop, SLOT(quit()));

    timer.start(ms); //your predefined timeout

    loop.exec();

    if (timer.isActive()) //replay received before timer, you can then get replay form network access manager and do whatever you want with it
    {

    }
    else //timer elapsed, no replay from client, ups
    {

    }
}


