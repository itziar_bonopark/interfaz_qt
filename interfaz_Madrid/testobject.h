#ifndef TESTOBJECT_H
#define TESTOBJECT_H

#include <qobject.h>
#include<QDebug>

class QWebView;
class QWebFrame;

class MyApi : public QObject
{
    Q_OBJECT
public:
    MyApi( QObject *parent );

    void setWebView( QWebView *view );
    void setIdEstacion(int id);


public slots:
    void getID();
    void click();
    void setPapel(bool papel);
    void setTarjetas(bool tarjetas);
    bool getPapel();
    bool getTarjetas();
    void esperandoRecogidaAbono();
    void obtenerTarjeta();
    void VentanaAverias();
    void imprimirTrayecto(QString datos);
    void ExpulsarTarjeta();
    void ReclamarTarjeta();
    void mandarcorreo(QString datos);
    void imprimirCompra(QString datos);
    void imprimirRecarga(QString datos);
    void imprimirTurista(QString datos);
    void crearAbonoRecogida(QString datos);
    void continuar();

    void compraAbono(QString datos);
    void devolucionPreautorizacion(QString datos);
    void imprimirTuristaDenegada();
    void imprimirAnulacion();

    void recargaAbono(QString datos);
    void devolucionRecarga(QString datos);
    void imprimirRecargaDenegada();
    void imprimirDevolucion();

private slots:
    void attachObject();


signals:
    void sgnClick();
    void sgnRecogidaAbono();
    void sgnObtenerTarjeta();
    void sgnVentanaAverias();
    void sgnIdEstacion();
    void sgnImprimirTrayecto(QString rfid,QString idestacion_desenganche,QString idestacion_enganche,QString importe,QString tiempo_trayecto,QString fecha_desenganche,QString fecha_enganche,QString dni);
    void sgnExpulsarTarjeta();
    void sgnReclamarTarjeta();
    void sgnTutorial1();
    void sgnlanzarTeclado();
    void sgnocultarTeclado();
    void sgnmandarcorreo(QString mail,QString tipo,QString fecha_caducidad,QString id_usuario,QString nombre,QString apellido1,QString apellido2,QString password,QString telefono);
    void sgncondiciones_uso();
    void sgnImprimirCompra(QString rfid,QString dni,QString importe_abono,QString saldo_inicial);
    void sgnImprimirRecarga(QString rfid,QString dni,QString recarga);
    void sgnImprimirTurista(QString rfid,QString dni,QString duracion,QString importe);
    void sgncrearAbonoRecogida(QString dni,QString tipo,QString estado,QString tipo_documento,QString idrfid,QString duracion,QString saldo);
    void sgncontinuar();

    void sgncompraAbono(QString importe,QString tipo_transaccion);
    void sgndevolucionPreautorizacion(QString pedido,QString rts);
    void sgnImprimirTuristaDenegada();
    void sgnImprimirAnulacion();

    void sgnrecargaAbono(QString importe,QString tipo_transaccion);
    void sgndevolucionRecarga(QString pedido,QString rts,QString importe);
    void sgnImprimirRecargaDenegada();
    void sgnImprimirDevolucion();


private:
    QWebFrame *frame;
    bool papel;
    bool tarjetas;
    int idestacion;

};

#endif // TESTOBJECT_H

