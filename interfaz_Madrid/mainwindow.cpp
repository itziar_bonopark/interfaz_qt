#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    myapi = new MyApi( ui->webView );
    myapi->setWebView( ui->webView  );

    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, true);
    connect(this->myapi,SIGNAL(sgnRecogidaAbono()),this,SLOT(sltIntencionDeRecogerAbono()));
    connect(this->myapi,SIGNAL(sgnObtenerTarjeta()),this,SLOT(sltObtenerTarjeta()));
    connect(this->myapi,SIGNAL(sgnVentanaAverias()),this,SLOT(sltVentanaAverias()));
    connect(this->myapi,SIGNAL(sgnIdEstacion()),this,SLOT(sltGetIdEstacion()));
    connect(this->myapi,SIGNAL(sgnImprimirTrayecto(QString,QString,QString,QString,QString,QString,QString,QString)),this,SLOT(sltImprimirTrayecto(QString,QString,QString,QString,QString,QString,QString,QString)));
    connect(this->myapi,SIGNAL(sgnExpulsarTarjeta()),this,SLOT(sltExpulsarTarjeta()));
    connect(this->myapi,SIGNAL(sgnReclamarTarjeta()),this,SLOT(sltReclamarTarjeta()));
    connect(this->myapi,SIGNAL(sgnmandarcorreo(QString,QString,QString,QString,QString,QString,QString,QString,QString)),this,SLOT(sltmandarcorreo(QString,QString,QString,QString,QString,QString,QString,QString,QString)));
    connect(this->myapi,SIGNAL(sgnImprimirCompra(QString,QString,QString,QString)),this,SLOT(sltImprimirCompra(QString,QString,QString,QString)));
    connect(this->myapi,SIGNAL(sgnImprimirRecarga(QString,QString,QString)),this,SLOT(sltImprimirRecarga(QString,QString,QString)));
    connect(this->myapi,SIGNAL(sgnImprimirTurista(QString,QString,QString,QString)),this,SLOT(sltImprimirTurista(QString,QString,QString,QString)));
    connect(this->myapi,SIGNAL(sgncrearAbonoRecogida(QString,QString,QString,QString,QString,QString,QString)),this,SLOT(sltcrearAbonoRecogida(QString,QString,QString,QString,QString,QString,QString)));
    connect(this->myapi,SIGNAL(sgncontinuar()),this,SLOT(sltcontinuar()));

    connect(this->myapi,SIGNAL(sgncompraAbono(QString,QString)),this,SLOT(sltcompraAbono(QString,QString)));
    connect(this->myapi,SIGNAL(sgndevolucionPreautorizacion(QString,QString)),this,SLOT(sltdevolucionPreautorizacion(QString,QString)));
    connect(this->myapi,SIGNAL(sgnImprimirTuristaDenegada()),this,SLOT(sltImprimirTuristaDenegada()));
    connect(this->myapi,SIGNAL(sgnImprimirAnulacion()),this,SLOT(sltImprimirAnulacion()));

    connect(this->myapi,SIGNAL(sgnrecargaAbono(QString,QString)),this,SLOT(sltrecargaAbono(QString,QString)));
    connect(this->myapi,SIGNAL(sgndevolucionRecarga(QString,QString,QString)),this,SLOT(sltdevolucionRecarga(QString,QString,QString)));
    connect(this->myapi,SIGNAL(sgnImprimirRecargaDenegada()),this,SLOT(sltImprimirRecargaDenegada()));
    connect(this->myapi,SIGNAL(sgnImprimirDevolucion()),this,SLOT(sltImprimirDevolucion()));




    this->mImpresora=new CImpresora;

    this->mHilo=new Hilo;
    connect(this->mHilo,SIGNAL(sgnTarjetaLeida(QByteArray)),this,SLOT(sltTarjetaLeida(QByteArray)));
    connect(this->mHilo,SIGNAL(sgnRegresar()),this,SLOT(sltRegresar()));
    connect(this->mHilo,SIGNAL(sgnLeyendoTarjeta()),this,SLOT(sltLeyendoTarjeta()));
    connect(this->mHilo,SIGNAL(sgnErrorLecturaTarjeta()),this,SLOT(sltErrorLecturaTarjeta()));
    connect(this->mHilo,SIGNAL(sgnSiguientePantalla()),this,SLOT(sltSiguientePantalla()));
    connect(this->mHilo,SIGNAL(sgnTarjetaTragada(QByteArray)),this,SLOT(sltTarjetaTragada(QByteArray)));
    connect(this->mHilo,SIGNAL(sgnMostrarMenu()),this,SLOT(sltMostrarMenu()));

    this->mHilo->start();



    this->myapi->setIdEstacion(this->idestacion);


    this->timerSalvapantallas=new QTimer;
    connect(this->timerSalvapantallas,SIGNAL(timeout()),this,SLOT(sltTimeoutSalvapantallas()));
    connect(this->myapi,SIGNAL(sgnClick()),this,SLOT(sltActividad()));

    this->timer_tarjeta= new QTimer;
    connect(this->timer_tarjeta,SIGNAL(timeout()),this,SLOT(sltTimeoutComprobaciones()));
    this->timer_tarjeta->start(500);

    this->obtenerConfiguracion();
    ui->webView ->setUrl( QUrl( this->web_interfaz ) );

    this->resetTimer();
    connect(this->s,SIGNAL(sgnClickEnSalvapantallas()),this,SLOT(sltClickEnSalvapantallas()));


}
void MainWindow::sltRegresar()
{
    ui->webView ->setUrl( QUrl( this->web_interfaz+"/menu.php" ) );
}

void MainWindow::sltTimeoutComprobaciones()
{
    this->timer_tarjeta->stop();
    this->ComprobarPapel();
    this->ComprobarTarjetas();
    this->timer_tarjeta->start(500);
}

void MainWindow::sltMostrarMenu()
{
    ui->webView ->setUrl( QUrl( this->web_interfaz+ this->url_menu ) );
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::obtenerConfiguracion()
{
    QString m_sSettingsFile="../configuraciones/settings.ini";
    this->mSettings=new QSettings(m_sSettingsFile, QSettings::NativeFormat);

    this->idestacion=this->mSettings->value("idestacion").toInt();
    this->web_interfaz=this->mSettings->value("interfaz_web").toString();
    this->url_menu=this->mSettings->value("menu").toString();

    this->path_salvapantallas=this->mSettings->value("ruta_salvapantallas").toString();
    this->s= new Salvapantallas(this->path_salvapantallas);

    int tiempo_espera_recogida_tarjeta=this->mSettings->value("tiempo_espera_recogida_tarjeta","20000").toInt();
    this->mHilo->setTiempoRecogida(tiempo_espera_recogida_tarjeta);
    int tiempo_espera_introduccion_tarjeta=this->mSettings->value("tiempo_espera_introduccion_tarjeta","20000").toInt();
    this->mHilo->setTiempoIntroduccion(tiempo_espera_introduccion_tarjeta);
    this->mHilo->mDispensador->puerto=this->mSettings->value("puerto_dispensador","/dev/ttyS4").toString();
    this->mHilo->mDispensador->abrirPuerto();
    this->mHilo->mDispensador->start();

    QString ruta=this->mSettings->value("ruta_logo").toString();
    this->mImpresora->setRuta(ruta);
    this->mImpresora->puerto_impresora=this->mSettings->value("puerto_impresora","/dev/ttyS3").toString();
    this->mImpresora->abrirPuerto();

    QString driver  = this->mSettings->value("database_driver").toString();
    QString hostName  = this->mSettings->value("database_host").toString();
    QString databaseName  = this->mSettings->value("database_name").toString();
    QString user  = this->mSettings->value("database_user").toString();
    QString password  = this->mSettings->value("database_password").toString();


    database = QSqlDatabase::addDatabase(driver);
    database.setConnectOptions("MYSQL_OPT_RECONNECT=1");
    database.setHostName(hostName);
    database.setDatabaseName(databaseName);
    database.setUserName(user);
    database.setPassword(password);
    if(database.open())
    {
        //qDebug()<<"funciona";
    }
    else
    {
        qDebug() << "Error = " << database.lastError().text();

    }
}

void MainWindow::sltLeyendoTarjeta()
{
    qDebug()<<"Leyendo tarjeta";
    ui->webView->page()->mainFrame()->evaluateJavaScript("LeyendoTarjeta()");
}

void MainWindow::sltErrorLecturaTarjeta()
{
    ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorLecturaTarjeta()");
}
void MainWindow::sltcontinuar()
{
    this->mHilo->setMaquina(ESTADO_COMPROBANDO_TARJETA);
}

void MainWindow::sltSiguientePantalla()
{
    ui->webView->page()->mainFrame()->evaluateJavaScript("MostrarSiguientePantalla()");
}

void MainWindow::sltTimeoutSalvapantallas()
{
    this->timerSalvapantallas->stop();
    s->empezar();
    s->setWindowFlags(Qt::FramelessWindowHint);
    s->setWindowState( s->windowState() | Qt::WindowFullScreen);
    s->exec();

}
void MainWindow::sltClickEnSalvapantallas()
{
    ui->webView ->setUrl( QUrl( this->web_interfaz ) );
    this->resetTimer();
}
void MainWindow::resetTimer()
{
    this->timerSalvapantallas->stop();
    this->timerSalvapantallas->start(120000);
}
void MainWindow::sltActividad()
{
    //qDebug()<<"reset";
    this->resetTimer();
}
void MainWindow::sltExpulsarTarjeta()
{
    this->mHilo->semipoping();
    this->mHilo->setMaquina(ESTADO_DISPENSADOR_REPOSO);
}

void MainWindow::sltReclamarTarjeta()
{
    this->mHilo->reclaim();
    this->mHilo->setMaquina(ESTADO_DISPENSADOR_REPOSO);
}

void MainWindow::sltIntencionDeRecogerAbono()
{
    bool tarjetas=this->ComprobarTarjetas();
    bool papel=this->ComprobarPapel();
    if(tarjetas)
    {
        this->mHilo->semidispensing();
        this->mHilo->setMaquina(ESTADO_ESPERANDO_A_TENER_TARJETA_EN_POSICION_DE_LECTURA);
    }
    else
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("NoHayTarjetas()");
    }

}

void MainWindow::sltRecogidaAbonoMal()
{
    ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorDesconocido()");

}

bool MainWindow::ComprobarTarjetas()
{
    this->tarjetas_dispensador=this->mHilo->mDispensador->getTarjetas_Dispensador();
    this->myapi->setTarjetas(tarjetas_dispensador);
    return tarjetas_dispensador;

}

bool MainWindow::ComprobarPapel()
{
    this->papel_impresora=this->mImpresora->gethaypapel();
    this->myapi->setPapel(papel_impresora);
    return papel_impresora;
}


void MainWindow::sltTarjetaTragada(QByteArray idrfid)
{
    qWarning()<< "ALARMA:La tarjeta "+idrfid.toHex() + " se ha tragado por seguridad en el totem "+ QString::number(this->idestacion);
    ui->webView->page()->mainFrame()->evaluateJavaScript("TarjetaTragada()");

}

void MainWindow::sltObtenerTarjeta()
{
    this->mHilo->setMaquina(ESTADO_ESPERANDO_TARJETA_USUARIO);
}

void MainWindow::sltTarjetaLeida(QByteArray identificacion_tarjeta)
{
    QByteArray rfid=identificacion_tarjeta;
    qDebug()<<"La tarjeta del usuario es: "+rfid.toHex();
    ui->webView->page()->mainFrame()->evaluateJavaScript("TarjetaLeida(\""+rfid.toHex()+"\")");
}

void MainWindow::sltImprimirTrayecto(QString rfid,QString idestacion_desenganche,QString idestacion_enganche,QString importe,QString tiempo_trayecto,QString fecha_desenganche,QString fecha_enganche,QString dni)
{
    bool papel=this->ComprobarPapel();
    if(!papel)
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("ImprimiendoTicket()");

        /*QString mensaje="9#"+rfid+"#"+idestacion_desenganche+"#"+idestacion_enganche+"#"+importe+"#"+tiempo_trayecto+"#"+fecha_desenganche+"#"+fecha_enganche+"#"+dni;

        this->sktPagos=new QTcpSocket;
        if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
        {
            this->sktPagos->connectToHost("localhost",88888);
            if(this->sktPagos->waitForConnected(500))
            {
                this->sktPagos->write(mensaje.toAscii());
                if(this->sktPagos->waitForBytesWritten(500))
                {
                   // qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                    this->sktPagos->close();
                }
            }
        }
        else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
                //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                this->sktPagos->close();
            }
        }*/

        QList<QString> desenganche=fecha_desenganche.split(" ");
        QList<QString> fecha=desenganche[0].split("-");
        QString hora=desenganche[1];
        QString year=fecha[0];
        QString month=fecha[1];
        QString day=fecha[2];
        QString desenganche_fecha=day+"-"+month+"-"+year+" "+hora;

        QList<QString> enganche=fecha_enganche.split(" ");
        QList<QString> fecha1=enganche[0].split("-");
        QString hora1=enganche[1];
        QString year1=fecha1[0];
        QString month1=fecha1[1];
        QString day1=fecha1[2];
        QString enganche_fecha=day1+"-"+month1+"-"+year1+" "+hora1;

        this->mImpresora->imprimirLogo("imagenes/logoBicimad-01.png");
        QString texto="     BiciMAD.                Fecha:"+QDateTime::currentDateTime().toString("dd/MM/yyyy")+"\n\n\n";
        texto.append("      'JUSTIFICANTE TRAYECTO REALIZADO'\n\n\n");
        texto.append("   DNI:"+dni+"          RFID:"+rfid.mid(0,4)+"****\n\n\n");
        texto.append("ESTACION ORIGEN:"+idestacion_desenganche+"\n\nFECHA INICIO:"+desenganche_fecha+"\n\n");
        texto.append("ESTACION DESTINO:"+idestacion_enganche+"\n\nFECHA FIN:"+enganche_fecha+"\n\n");
        texto.append("DURACION:"+tiempo_trayecto+"    IMPORTE:"+importe+" euros\n\n");
        this->mImpresora->imprimirTexto(texto);

    }
    else
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("NoHayPapel()");
    }

}

void MainWindow::sltImprimirCompra(QString rfid,QString dni,QString importe_abono,QString saldo_inicial)
{
    bool papel=this->ComprobarPapel();
    if(!papel)
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("ImprimiendoTicket()");
        bool ok;
        ok=true;

        if(ok)
        {
            QString texto="     BiciMAD.                Fecha:"+QDateTime::currentDateTime().toString("dd/MM/yyyy")+"\n\n\n";
            texto.append("      'JUSTIFICANTE PAGO REALIZADO'\n\n\n");
            texto.append("   DNI:"+dni+"          RFID:"+rfid.mid(0,4)+"****\n\n\n");
            texto.append("IMPORTE ABONO/TARJETA:"+importe_abono+" euros\n\n");
            texto.append("SALDO INICIAL:"+saldo_inicial+" euros\n\n");
            this->mImpresora->imprimirTexto(texto);
        }
    }
    else
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("NoHayPapel()");
    }

}

void MainWindow::sltImprimirRecarga(QString rfid,QString dni,QString recarga)
{
    bool papel=this->ComprobarPapel();
    if(!papel)
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("ImprimiendoTicket()");

        QString mensaje="9#";

        this->sktPagos=new QTcpSocket;
        if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
        {
            this->sktPagos->connectToHost("localhost",88888);
            if(this->sktPagos->waitForConnected(500))
            {
                this->sktPagos->write(mensaje.toAscii());
                if(this->sktPagos->waitForBytesWritten(500))
                {
                   // qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                    this->sktPagos->close();
                }
            }
        }
        else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
                //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                this->sktPagos->close();
            }
        }
    }
    else
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("NoHayPapel()");
    }

}


void MainWindow::sltImprimirTuristaDenegada()
{
    bool papel=this->ComprobarPapel();
    if(!papel)
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("ImprimiendoTicket()");

        QString mensaje="7#";

        this->sktPagos=new QTcpSocket;
        if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
        {
            this->sktPagos->connectToHost("localhost",88888);
            if(this->sktPagos->waitForConnected(500))
            {
                this->sktPagos->write(mensaje.toAscii());
                if(this->sktPagos->waitForBytesWritten(500))
                {
                   // qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                    this->sktPagos->close();
                }
            }
        }
        else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
                //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                this->sktPagos->close();
            }
        }
    }
    else
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("NoHayPapel()");
    }
}

void MainWindow::sltImprimirAnulacion()
{
    bool papel=this->ComprobarPapel();
    if(!papel)
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("ImprimiendoTicket()");

        QString mensaje="8#";

        this->sktPagos=new QTcpSocket;
        if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
        {
            this->sktPagos->connectToHost("localhost",88888);
            if(this->sktPagos->waitForConnected(500))
            {
                this->sktPagos->write(mensaje.toAscii());
                if(this->sktPagos->waitForBytesWritten(500))
                {
                   // qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                    this->sktPagos->close();
                }
            }
        }
        else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
                //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                this->sktPagos->close();
            }
        }
    }
    else
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("NoHayPapel()");
    }
}

void MainWindow::sltImprimirTurista(QString rfid,QString dni,QString duracion,QString importe)
{
    bool papel=this->ComprobarPapel();
    if(!papel)
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("ImprimiendoTicket()");

        QString mensaje="6#";

        this->sktPagos=new QTcpSocket;
        if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
        {
            this->sktPagos->connectToHost("localhost",88888);
            if(this->sktPagos->waitForConnected(500))
            {
                this->sktPagos->write(mensaje.toAscii());
                if(this->sktPagos->waitForBytesWritten(500))
                {
                   // qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                    this->sktPagos->close();
                }
            }
        }
        else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
                //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                this->sktPagos->close();
            }
        }
    }
    else
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("NoHayPapel()");
    }

}

void MainWindow::sltGetIdEstacion()
{
    QSqlQuery q;
    QString sentencia="select nombre,longitud,latitud,luz,numero_estacion from estacion where idestacion="+QString::number(this->idestacion);
    //qDebug()<<sentencia;
    if(q.exec(sentencia))
    {
        if(q.next())
        {
            double lng=q.value(1).toDouble();
            double ltd=q.value(2).toDouble();
            int luz=q.value(3).toInt();
            QString numero_estacion=q.value(4).toString();
            ui->webView->page()->mainFrame()->evaluateJavaScript("ObtenerIdEstacion(\""+QString::number(this->idestacion)+"\",\""+QString::number(lng)+"\",\""+QString::number(ltd)+"\",\""+QString::number(luz)+"\",\""+numero_estacion+"\")");

        }
    }
    else
    {
        qDebug()<<q.lastError().text();
    }

}

void MainWindow::sltVentanaAverias()
{
    int basesTotales=0;
    int basesEnganchados=0;
    int envio=0; //NO_SE_PUEDE_NI_FALLO_EN_ORIGEN_NI_FALLO_DESTINO;
    QSqlQuery q;
    QString sentencia="select estado from base where idestacion="+QString::number(this->idestacion);
    if(q.exec(sentencia))
    {
        while(q.next())
        {
            basesTotales++;
            int estado=q.value(0).toInt();
            if( (estado==3)||(estado==7))
            {
                basesEnganchados++;
            }

        }
    }
    if(basesTotales>0)
    {
         if(basesEnganchados==basesTotales)
         {
             envio=1; //FALLO_EN_DESTINO;
         }
         else if(basesEnganchados==0)
         {
             envio=2; //FALLO_EN_ORIGEN;
         }

    }
    QString envio_texto=QString::number(envio);
    ui->webView->page()->mainFrame()->evaluateJavaScript("MostrarAveria(\""+envio_texto+"\")");

}

void MainWindow::sendMail(QString to, QString subject, QString body)
{
    SmtpClient smtp("smtp.gmail.com", 465, SmtpClient::SslConnection);

    // We need to set the username (your email address) and the password
    // for smtp authentification.

    smtp.setUser("noreplybonopark@gmail.com");
    smtp.setPassword("nuevacuenta");

    // Now we create a MimeMessage object. This will be the email.

    MimeMessage message;

    message.setSender(new EmailAddress("noreplybonopark@gmail.com", "BiciMAD"));
    message.addRecipient(new EmailAddress(to, to));
    message.setSubject(subject);

    // Now add some text to the email.
    // First we create a MimeText object.

    MimeText text;

    text.setText(body);

    // Now add it to the mail

    message.addPart(&text);

    // Now we can send the mail

    smtp.connectToHost();
    smtp.login();
    smtp.sendMail(message);
    smtp.quit();
}

void MainWindow::sltmandarcorreo(QString mail,QString tipo,QString fecha_caducidad,QString id_usuario,QString nombre,QString apellido1,QString apellido2,QString password,QString telefono)
{

    QString asunto="Alta Abonado BICIMAD-Ayuntamiento de Madrid";
    QString mensaje=QString::fromUtf8("Código de identificación/código de usuario: ")+id_usuario+"\nNombre y apellidos:"+nombre+" "+apellido1+" "+apellido2+"\n";
    mensaje+=QString::fromUtf8("Gracias por darse de alta como abonado de BiciMAD.\nLa fecha de vigencia será hasta ")+fecha_caducidad+"\n\n\n\n";
    mensaje+=QString::fromUtf8("Estamos a su disposición en www.madrid.es/contactar, en nuestras Oficinas de Atención a la\n");
    mensaje+=QString::fromUtf8("Ciudadanía (Línea Madrid), en el Servicio de Atención Telefónica 010-Línea Madrid (accesible\n");
    mensaje+=QString::fromUtf8("marcando 010 desde nuestra ciudad o bien 91 529 82 10) y en nuestra cuenta de la red social\n");
    mensaje+=QString::fromUtf8("Twitter @lineamadrid.");
    this->sendMail(mail,asunto,mensaje);

    QString asunto2="Comunicación contraseña BICIMAD-Ayuntamiento de Madrid";
    QString mensaje2=QString::fromUtf8("Le indicamos que su contraseña para operar en la página web de BICIMAD es el siguiente:\nCONTRASEÑA: ")+password+"\n\n\n\n";
    mensaje2+=QString::fromUtf8("Estamos a su disposición en www.madrid.es/contactar, en nuestras Oficinas de Atención a la\n");
    mensaje2+=QString::fromUtf8("Ciudadanía (Línea Madrid), en el Servicio de Atención Telefónica 010-Línea Madrid (accesible\n");
    mensaje2+=QString::fromUtf8("marcando 010 desde nuestra ciudad o bien 91 529 82 10) y en nuestra cuenta de la red social\n");
    mensaje2+=QString::fromUtf8("Twitter @lineamadrid.");
    this->sendMail(mail,asunto2,mensaje2);

   /* QString sms2=telefono+"#ALTA ABONADO. CLAVE "+password+". CONDICIONES PARTICIPACIÓN Y DUDAS PROTECCIÓN DATOS WWW.MADRID.ES/...";
    this->sktSMS=new QTcpSocket;
    if(this->sktSMS->state()==QTcpSocket::UnconnectedState)
    {
        this->sktSMS->connectToHost("localhost",2040);
        if(this->sktSMS->waitForConnected(500))
        {
            this->sktSMS->write(sms2.toAscii());
            if(this->sktSMS->waitForBytesWritten(500))
            {
                qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
            }
        }
    }
    else if(this->sktSMS->state()==QTcpSocket::ConnectedState)
    {
        this->sktSMS->write(sms2.toAscii());
        if(this->sktSMS->waitForBytesWritten(500))
        {
            qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
        }
    }
    usleep(100000);
    QString sms=telefono+"#IDENTIFICACIÓN "+id_usuario+". ALTA ABONADO HASTA "+fecha_caducidad+". CONDICIONES PARTICIPACIÓN Y DUDAS PROTECCIÓN DATOS WWW.MADRID.ES/...";
    this->sktSMS=new QTcpSocket;
    if(this->sktSMS->state()==QTcpSocket::UnconnectedState)
    {
        this->sktSMS->connectToHost("localhost",2040);
        if(this->sktSMS->waitForConnected(500))
        {
            this->sktSMS->write(sms.toAscii());
            if(this->sktSMS->waitForBytesWritten(500))
            {
                qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
            }
        }
    }
    else if(this->sktSMS->state()==QTcpSocket::ConnectedState)
    {
        this->sktSMS->write(sms.toAscii());
        if(this->sktSMS->waitForBytesWritten(500))
        {
            qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
        }
    }*/

}


void MainWindow::sltcrearAbonoRecogida(QString dni,QString tipo,QString estado,QString tipo_documento,QString idrfid,QString duracion,QString saldo)
{

    this->sltExpulsarTarjeta();
    QString sentencia_rfid="insert into rfid (idrfid) values ('"+idrfid+"')";
    QSqlQuery q2;
    if(q2.exec(sentencia_rfid))
    {
        qDebug()<<"insertado rfid";
    }
    QString sentencia_usuario="insert into usuario (dni,tipo,estado,tipo_documento)values('"+dni+"',"+tipo+","+estado+","+tipo_documento+")";
    QSqlQuery q3;
    if(q3.exec(sentencia_usuario))
    {
        qDebug()<<"faltaba usuario";
    }
    QString sentencia_abono="insert into abono (dni,tipo,idrfid,duracion,saldo) values ('"+dni+"',"+tipo+",'"+idrfid+"',"+duracion+","+saldo+")";
    QSqlQuery q;
    if(q.exec(sentencia_abono))
    {
        qDebug()<<"abono insertado correctamente";
    }
    else
    {
        qDebug()<<sentencia_abono;
        qDebug()<<q.lastError().text();
    }
}

void MainWindow::sltcompraAbono(QString importe, QString tipo_transaccion)
{
    if(tipo_transaccion=="1")
    {
        QString respuesta;
        QString operacion;
        QString mensaje=tipo_transaccion+"#"+importe;

        this->sktPagos=new QTcpSocket;
        if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
        {
            this->sktPagos->connectToHost("localhost",88888);
            if(this->sktPagos->waitForConnected(500))
            {
                this->sktPagos->write(mensaje.toAscii());
                if(this->sktPagos->waitForBytesWritten(500))
                {
                    //qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                }
                if(this->sktPagos->waitForReadyRead(180000))
                {
                    QByteArray respuesta_socket=this->sktPagos->readAll();
                    operacion=respuesta_socket.constData();
                    this->sktPagos->close();
                }
                else
                {
                    this->sktPagos->close();
                }
            }
        }
        else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
                //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
            }
            if(this->sktPagos->waitForReadyRead(180000))
            {
                QByteArray respuesta_socket=this->sktPagos->readAll();
                operacion=respuesta_socket.constData();
                this->sktPagos->close();
            }
            else
            {
                this->sktPagos->close();
            }
        }


        if(operacion!="")
        {
            QList<QString>transaccion=operacion.split("//");

            short shStatus=transaccion[0].toShort();
            //qDebug()<<shStatus;

            if(shStatus==0)
            {
                qDebug()<<"Operacion realizada correctamente. Hay que comprobar la transaccion";

                if(transaccion[1]=="0")
                {
                    qDebug()<<"operacion denegada";
                    QString causa=transaccion[2];
                    qDebug()<<causa;
                    ui->webView->page()->mainFrame()->evaluateJavaScript("OperacionDenegada(\""+causa+"\")");
                }
                else
                {
                    respuesta=transaccion[2];
                    qDebug()<< "operacion correcta";
                    ui->webView->page()->mainFrame()->evaluateJavaScript("OperacionCorrecta(\""+respuesta+"\")");

                }

            }
            else
            {
                qDebug()<<"Error: "+QString::number(shStatus);
                ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorOperacion(\""+QString::number(shStatus)+"\")");
            }
        }
        else
        {
            qDebug()<<"No habido respuesta";
            ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorOperacion('Sin respuesta')");
        }

    }
}

void MainWindow::sltdevolucionPreautorizacion(QString pedido,QString rts)
{
    QString respuesta;
    QString operacion;
    QString mensaje="2#"+pedido+"#"+rts;

    this->sktPagos=new QTcpSocket;
    if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
    {
        this->sktPagos->connectToHost("localhost",88888);
        if(this->sktPagos->waitForConnected(500))
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
               // qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
            }
            if(this->sktPagos->waitForReadyRead(180000))
            {
                QByteArray respuesta_socket=this->sktPagos->readAll();
                operacion=respuesta_socket.constData();
                this->sktPagos->close();
            }
            else
            {
                this->sktPagos->close();
            }
        }
    }
    else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
    {
        this->sktPagos->write(mensaje.toAscii());
        if(this->sktPagos->waitForBytesWritten(500))
        {
            //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
        }
        if(this->sktPagos->waitForReadyRead(180000))
        {
            QByteArray respuesta_socket=this->sktPagos->readAll();
            operacion=respuesta_socket.constData();
            this->sktPagos->close();
        }
        else
        {
            this->sktPagos->close();
        }
    }

    if(operacion!="")
    {
        QList<QString>transaccion=operacion.split("//");

        short shStatus=transaccion[0].toShort();
        //qDebug()<<shStatus;

        if(shStatus==0)
        {
            qDebug()<<"Operacion realizada correctamente. Hay que comprobar la transaccion";


            if(transaccion[1]=="0")
            {
                qDebug()<<"operacion denegada";
                QString causa=transaccion[2];
                qDebug()<<causa;
                ui->webView->page()->mainFrame()->evaluateJavaScript("OperacionDenegada(\""+causa+"\")");
            }
            else
            {
                respuesta=transaccion[2];
                qDebug()<< "operacion correcta";
                ui->webView->page()->mainFrame()->evaluateJavaScript("OperacionCorrecta(\""+respuesta+"\")");

            }

        }
        else
        {
            qDebug()<<"Error: "+QString::number(shStatus);
            ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorOperacion(\""+QString::number(shStatus)+"\")");
        }
    }
    else
    {
        qDebug()<<"Sin respuesta";
        ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorOperacion('Sin Respuesta')");
    }
}

void MainWindow::sltrecargaAbono(QString importe, QString tipo_transaccion)
{
    if(tipo_transaccion=="4")
    {
        QString respuesta;
        QString operacion;
        QString mensaje=tipo_transaccion+"#"+importe;

        this->sktPagos=new QTcpSocket;
        if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
        {
            this->sktPagos->connectToHost("localhost",88888);
            if(this->sktPagos->waitForConnected(500))
            {
                this->sktPagos->write(mensaje.toAscii());
                if(this->sktPagos->waitForBytesWritten(500))
                {
                    //qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                }
                if(this->sktPagos->waitForReadyRead(180000))
                {
                    QByteArray respuesta_socket=this->sktPagos->readAll();
                    operacion=respuesta_socket.constData();
                    this->sktPagos->close();
                }
                else
                {
                    this->sktPagos->close();
                }
            }
        }
        else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
                //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
            }
            if(this->sktPagos->waitForReadyRead(180000))
            {
                QByteArray respuesta_socket=this->sktPagos->readAll();
                operacion=respuesta_socket.constData();
                this->sktPagos->close();
            }
            else
            {
                this->sktPagos->close();
            }
        }


        if(operacion!="")
        {
            QList<QString>transaccion=operacion.split("//");

            short shStatus=transaccion[0].toShort();
            //qDebug()<<shStatus;

            if(shStatus==0)
            {
                qDebug()<<"Operacion realizada correctamente. Hay que comprobar la transaccion";

                if(transaccion[1]=="0")
                {
                    qDebug()<<"operacion denegada";
                    QString causa=transaccion[2];
                    qDebug()<<causa;
                    ui->webView->page()->mainFrame()->evaluateJavaScript("OperacionDenegada(\""+causa+"\")");
                }
                else
                {
                    respuesta=transaccion[2];
                    qDebug()<< "operacion correcta";
                    ui->webView->page()->mainFrame()->evaluateJavaScript("OperacionCorrecta(\""+respuesta+"\")");

                }

            }
            else
            {
                qDebug()<<"Error: "+QString::number(shStatus);
                ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorOperacion(\""+QString::number(shStatus)+"\")");
            }
        }
        else
        {
            qDebug()<<"No habido respuesta";
            ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorOperacion('Sin respuesta')");
        }

    }
}
void MainWindow::sltdevolucionRecarga(QString pedido,QString rts,QString importe)
{
    QString respuesta;
    QString operacion;
    QString mensaje="5#"+pedido+"#"+rts+"#"+importe;

    this->sktPagos=new QTcpSocket;
    if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
    {
        this->sktPagos->connectToHost("localhost",88888);
        if(this->sktPagos->waitForConnected(500))
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
               // qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
            }
            if(this->sktPagos->waitForReadyRead(180000))
            {
                QByteArray respuesta_socket=this->sktPagos->readAll();
                operacion=respuesta_socket.constData();
                this->sktPagos->close();
            }
            else
            {
                this->sktPagos->close();
            }
        }
    }
    else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
    {
        this->sktPagos->write(mensaje.toAscii());
        if(this->sktPagos->waitForBytesWritten(500))
        {
            //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
        }
        if(this->sktPagos->waitForReadyRead(180000))
        {
            QByteArray respuesta_socket=this->sktPagos->readAll();
            operacion=respuesta_socket.constData();
            this->sktPagos->close();
        }
        else
        {
            this->sktPagos->close();
        }
    }

    if(operacion!="")
    {
        QList<QString>transaccion=operacion.split("//");

        short shStatus=transaccion[0].toShort();
        //qDebug()<<shStatus;

        if(shStatus==0)
        {
            qDebug()<<"Operacion realizada correctamente. Hay que comprobar la transaccion";


            if(transaccion[1]=="0")
            {
                qDebug()<<"operacion denegada";
                QString causa=transaccion[2];
                qDebug()<<causa;
                ui->webView->page()->mainFrame()->evaluateJavaScript("OperacionDenegada(\""+causa+"\")");
            }
            else
            {
                respuesta=transaccion[2];
                qDebug()<< "operacion correcta";
                ui->webView->page()->mainFrame()->evaluateJavaScript("OperacionCorrecta(\""+respuesta+"\")");

            }

        }
        else
        {
            qDebug()<<"Error: "+QString::number(shStatus);
            ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorOperacion(\""+QString::number(shStatus)+"\")");
        }
    }
    else
    {
        qDebug()<<"Sin respuesta";
        ui->webView->page()->mainFrame()->evaluateJavaScript("ErrorOperacion('Sin Respuesta')");
    }
}

void MainWindow::sltImprimirRecargaDenegada()
{
    bool papel=this->ComprobarPapel();
    if(!papel)
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("ImprimiendoTicket()");

        QString mensaje="10#";

        this->sktPagos=new QTcpSocket;
        if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
        {
            this->sktPagos->connectToHost("localhost",88888);
            if(this->sktPagos->waitForConnected(500))
            {
                this->sktPagos->write(mensaje.toAscii());
                if(this->sktPagos->waitForBytesWritten(500))
                {
                   // qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                    this->sktPagos->close();
                }
            }
        }
        else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
                //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                this->sktPagos->close();
            }
        }
    }
    else
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("NoHayPapel()");
    }
}

void MainWindow::sltImprimirDevolucion()
{
    bool papel=this->ComprobarPapel();
    if(!papel)
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("ImprimiendoTicket()");

        QString mensaje="11#";

        this->sktPagos=new QTcpSocket;
        if(this->sktPagos->state()==QTcpSocket::UnconnectedState)
        {
            this->sktPagos->connectToHost("localhost",88888);
            if(this->sktPagos->waitForConnected(500))
            {
                this->sktPagos->write(mensaje.toAscii());
                if(this->sktPagos->waitForBytesWritten(500))
                {
                   // qDebug()<<"Enviado mensaje"<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                    this->sktPagos->close();
                }
            }
        }
        else if(this->sktPagos->state()==QTcpSocket::ConnectedState)
        {
            this->sktPagos->write(mensaje.toAscii());
            if(this->sktPagos->waitForBytesWritten(500))
            {
                //qDebug()<<"Enviado mensaje "<<QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss:zzz");
                this->sktPagos->close();
            }
        }
    }
    else
    {
        ui->webView->page()->mainFrame()->evaluateJavaScript("NoHayPapel()");
    }
}


