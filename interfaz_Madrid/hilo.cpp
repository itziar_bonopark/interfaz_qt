#include "hilo.h"

Hilo::Hilo(QObject *parent) :
    QThread(parent)
{
    this->fallos_lectura=0;
    this->lecturaFinalizada=false;
    this->mDispensador=new neoDispensador;
    this->maq=ESTADO_DISPENSADOR_REPOSO;
    connect(this->mDispensador,SIGNAL(sgnLecturaFinalizadaDelBloqueYsector(QByteArray)),this,SLOT(sltLecturaFinalizadaDeBloqueYsector(QByteArray)));
    //this->mDispensador->puerto=this->puerto_dispensador;
    //this->mDispensador->abrirPuerto();
    //this->mDispensador->start();

}

void Hilo::setTiempoRecogida(int m)
{
    this->tiempo_espera_recogida_tarjeta=m;
}

void Hilo::setTiempoIntroduccion(int m)
{
    this->tiempo_espera_introduccion_tarjeta=m;
}

void Hilo::setMaquina(int m)
{
    this->fechaCambioEstado=QDateTime::currentDateTime();
    QString texto;
    switch(m)
    {

    case ESTADO_DISPENSADOR_REPOSO :
    {
        texto="REPOSO";
        break;
    }
    case ESTADO_ESPERANDO_A_TENER_TARJETA_EN_POSICION_DE_LECTURA :
    {
        texto="ESPERA A POSICION LECTURA";
        break;
    }
    case ESTADO_ESPERANDO_A_LEER_RFID :
    {
        this->fallos_lectura=0;
        texto="ESPERA RFID";
        break;
    }
    case ESTADO_ESPERANDO_A_TENER_DATOS_LEIDOS :
    {
        this->fallos_lectura=0;
        texto="ESPERA A DATOS";
        break;
    }
    case  ESTADO_ESPERANDO_A_RETIRAR_TARJETA :
    {
        texto="ESPERANDO RECOGER TARJETA";
        break;
    }

    case ESTADO_ESPERANDO_CREACION_ABONO:
    {
        texto="ESPERANDO CREACION ABONO CORRECTA";
        break;
    }
    case ESTADO_ESPERANDO_TARJETA_USUARIO:
    {
        texto="ESPERANDO TARJETA DEL USUARIO";
        break;
    }
    case ESTADO_ESPERANDO_RFID_USUARIO:
    {
        texto="ESPERANDO LEER RFID USUARIO";
        break;
    }
    case ESTADO_ESPERANDO_RFID_CORRECTO:
    {
        texto="LEYENDO RFID USUARIO";
        break;
    }
    case ESTADO_ESPERANDO_RECOGIDA_USUARIO:
    {
        texto="ESPERANDO RECOGIDA USUARIO";
        break;
    }
    case ESTADO_COMPROBANDO_TARJETA:
    {
        texto="SE HA RETIRADO LA TARJETA?";
        break;
    }


    }

    qDebug()<<texto;
    this->maq=m;
}

int Hilo::getMaquina()
{
    return this->maq;
}
void Hilo::sltLecturaFinalizadaDeBloqueYsector(QByteArray msg)
{
    this->lecturaFinalizada=true;
    this->ultimaLectura=msg;
}

void Hilo::run()
{
    QDateTime antes;
    this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
    while(true)
    {
        antes=this->fechaCambioEstado;
        msleep(100);
        switch(this->maq)
        {
        case ESTADO_MENU_USUARIO:
        {
            break;
        }
        case ESTADO_ESPERANDO_A_RETIRAR_TARJETA:
        {
            if((!this->mDispensador->getbTO3CardDetected())&&(!this->mDispensador->getbTO2CardDetected()))
            {
                 this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
            }
            else
            {
                QDateTime ahora=QDateTime::currentDateTime();
                int dif=this->fechaCambioEstado.msecsTo(ahora);
                if(dif>this->tiempo_espera_recogida_tarjeta)
                {
                    if((this->mDispensador->getbTO2CardDetected()) && (!this->mDispensador->getbTO1CardDetected())&&(!this->mDispensador->getbTO3CardDetected()))
                    {
                        this->mDispensador->terminate();
                        this->reclaim();
                        this->mDispensador->reclaim();
                        this->mDispensador->start();
                        this->resetDispensador();

                        this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                        emit this->sgnTarjetaTragada(this->identificador_tarjeta);
                    }
                    if((!this->mDispensador->getbTO2CardDetected()) && (!this->mDispensador->getbTO1CardDetected())&&(this->mDispensador->getbTO3CardDetected()))
                    {
                        this->mDispensador->terminate();
                        this->reclaim();
                        this->mDispensador->reclaim();
                        this->mDispensador->start();
                        this->resetDispensador();

                        this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                        emit this->sgnTarjetaTragada(this->identificador_tarjeta);

                    }

                }
            }
            break;
        }
        case ESTADO_DISPENSADOR_REPOSO:
        {

            if((this->mDispensador->getbTO3CardDetected())&&(this->mDispensador->getbTO2CardDetected()))
            {
                //this->setMaquina(ESTADO_ESPERANDO_A_RETIRAR_TARJETA);
                //this->semipoping();
                this->sgnMostrarMenu();
                this->setMaquina(ESTADO_MENU_USUARIO);


            }
            else  if((this->mDispensador->getbTO2CardDetected())&&(!this->mDispensador->getbTO1CardDetected())&&(!this->mDispensador->getbTO3CardDetected()))
            {
                this->resetDispensador();
                this->setMaquina(ESTADO_ESPERANDO_A_RETIRAR_TARJETA);
                this->reclaim();
            }
            else if(this->mDispensador->getbStackHolderEmpty())
            {
                this->mDispensador->setTarjetas_Dipensador(false);

            }
            else if(!this->mDispensador->getbStackHolderEmpty())
            {
               this->mDispensador->setTarjetas_Dipensador(true);
            }


            break;
        }
        case ESTADO_ESPERANDO_A_TENER_TARJETA_EN_POSICION_DE_LECTURA:
        {
            if((this->mDispensador->getbTO2CardDetected())&&(this->mDispensador->getbTO3CardDetected()))
            {
                this->mDispensador->limpiarLista();
                this->request();
                this->anticol();
                this->setMaquina(ESTADO_ESPERANDO_A_LEER_RFID);
            }
            else
            {

                QDateTime ahora=QDateTime::currentDateTime();
                int dif=this->fechaCambioEstado.msecsTo(ahora);

                if(dif>this->tiempo_espera_introduccion_tarjeta)
                {
                    this->reclaim();
                    this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                    qDebug()<<"Se ha pasado timeout. Problemas con el dispensador";
                    emit this->sgnErrorLecturaTarjeta();

                }

            }
            break;
        }
        case ESTADO_ESPERANDO_A_LEER_RFID:
        {
            QByteArray idrfid=this->mDispensador->getRfid();
            if(idrfid.toHex()!="")
            {
                qDebug()<<"rfid:"<<idrfid.toHex();
                this->seleccionar(idrfid);
                QByteArray key;
                for(int i=0;i<6;i++)key.append(0xFF);
                this->verificar(key,0,0);
                this->leer(0,0);
                this->setMaquina(ESTADO_ESPERANDO_A_TENER_DATOS_LEIDOS);
            }
            else
            {
                QDateTime ahora=QDateTime::currentDateTime();
                int dif=this->fechaCambioEstado.msecsTo(ahora);
                if(dif>10000)
                {
                    this->fallos_lectura++;
                    if(fallos_lectura==3)
                    {
                        this->reclaim();
                        this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                        qDebug()<<"Se ha pasado timeout. Problemas con el dispensador";
                        emit this->sgnErrorLecturaTarjeta();

                    }
                    else
                    {
                        this->reclaim();
                        this->setMaquina(ESTADO_ESPERANDO_A_TENER_TARJETA_EN_POSICION_DE_LECTURA);
                        qDebug()<<"No se ha podido leer tarjeta. Se traga y se dispensa otra nueva";
                        this->semidispensing();
                    }
                }
            }
            break;
        }
        case ESTADO_ESPERANDO_A_TENER_DATOS_LEIDOS:
        {
            if(this->lecturaFinalizada)
            {
                QByteArray identificador_tarjeta=this->ultimaLectura.mid(6,7);
                qDebug()<<"Identificador tarjeta: "+identificador_tarjeta.toHex();
                int length=identificador_tarjeta.toHex().length();
                if(length==14)
                {
                    emit this->sgnTarjetaLeida(identificador_tarjeta);
                    qDebug()<<"Tarjeta leida. Esperando a la creación del abono";
                    this->setMaquina(ESTADO_ESPERANDO_CREACION_ABONO);
                }
                else
                {
                    this->fallos_lectura++;
                    if(fallos_lectura==3)
                    {
                        this->reclaim();
                        this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                        qDebug()<<"Se ha pasado timeout. Problemas con el dispensador";
                    }
                    else
                    {
                        this->reclaim();
                        this->semidispensing();
                        this->setMaquina(ESTADO_ESPERANDO_A_TENER_TARJETA_EN_POSICION_DE_LECTURA);
                        qDebug()<<"No se ha podido leer tarjeta. Se traga y se dispensa otra nueva";
                    }
                }


            }
            else
            {
                QDateTime ahora=QDateTime::currentDateTime();
                int dif=this->fechaCambioEstado.msecsTo(ahora);
                if(dif>10000)
                {
                    this->fallos_lectura++;
                    if(fallos_lectura==3)
                    {
                        this->reclaim();
                        this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                        qDebug()<<"Se ha pasado timeout. Problemas con el dispensador";
                    }
                    else
                    {
                        this->reclaim();
                        this->semidispensing();
                        this->setMaquina(ESTADO_ESPERANDO_A_TENER_TARJETA_EN_POSICION_DE_LECTURA);
                        qDebug()<<"No se ha podido leer tarjeta. Se traga y se dispensa otra nueva";
                    }
                }
            }
            break;
        }
        case ESTADO_ESPERANDO_CREACION_ABONO:
        {

            break;
        }
        case ESTADO_ESPERANDO_TARJETA_USUARIO:
        {
            if((this->mDispensador->getbTO2CardDetected())&&(this->mDispensador->getbTO3CardDetected()))
            {
                this->mDispensador->limpiarLista();
                this->request();
                this->anticol();
                this->setMaquina(ESTADO_ESPERANDO_RFID_USUARIO);
                emit this->sgnLeyendoTarjeta();
            }
            else
            {

                QDateTime ahora=QDateTime::currentDateTime();
                int dif=this->fechaCambioEstado.msecsTo(ahora);
                if(dif>this->tiempo_espera_introduccion_tarjeta)
                {
                    this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                    qDebug()<<"Se ha pasado timeout. Mostrar pantalla inicio";

                }

            }
            break;
        }
        case ESTADO_ESPERANDO_RFID_USUARIO:
        {
            QByteArray idrfid=this->mDispensador->getRfid();
            if(idrfid.toHex()!="")
            {
                qDebug()<<"rfid:"<<idrfid.toHex();
                this->seleccionar(idrfid);
                QByteArray key;
                for(int i=0;i<6;i++)key.append(0xFF);
                this->verificar(key,0,0);
                this->leer(0,0);
                this->setMaquina(ESTADO_ESPERANDO_RFID_CORRECTO);
            }
            else
            {
                QDateTime ahora=QDateTime::currentDateTime();
                int dif=this->fechaCambioEstado.msecsTo(ahora);
                if(dif>10000)
                {
                    this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                    qDebug()<<"Se ha pasado timeout. Mostrar no se ha podido leer";
                    emit this->sgnErrorLecturaTarjeta();
                }
            }
            break;
        }
        case ESTADO_ESPERANDO_RFID_CORRECTO:
        {
            if(this->lecturaFinalizada)
            {
                this->identificador_tarjeta=this->ultimaLectura.mid(6,7);
                qDebug()<<"Identificador tarjeta: "+identificador_tarjeta.toHex();
                int length=identificador_tarjeta.toHex().length();
                if(length==14)
                {
                    emit this->sgnTarjetaLeida(identificador_tarjeta);
                    qDebug()<<"Tarjeta leida. Puede recoger su tarjeta";
                    this->semipoping();
                    this->setMaquina(ESTADO_ESPERANDO_RECOGIDA_USUARIO);
                }
                else
                {
                    qDebug()<<"Mostrar no se ha podido leer correctamente. Recoja su tarjeta e intentelo de nuevo";
                    this->semipoping();
                    this->setMaquina(ESTADO_ESPERANDO_RECOGIDA_USUARIO);
                    emit this->sgnErrorLecturaTarjeta();
                }
            }
            else
            {
                QDateTime ahora=QDateTime::currentDateTime();
                int dif=this->fechaCambioEstado.msecsTo(ahora);
                if(dif>10000)
                {
                    this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                    qDebug()<<"Se ha pasado timeout. Mostrar no se ha podido leer";
                }
            }
            break;
        }
        case ESTADO_ESPERANDO_RECOGIDA_USUARIO:
        {
            if((this->mDispensador->getbTO1CardDetected())&&(this->mDispensador->getbTO2CardDetected()))
            {
                qDebug()<<"ha vuelto a meter la tarjeta";
            }

            QDateTime ahora=QDateTime::currentDateTime();
            int dif=this->fechaCambioEstado.msecsTo(ahora);
             if(dif>this->tiempo_espera_recogida_tarjeta)
            {
                if((this->mDispensador->getbTO3CardDetected())&&(this->mDispensador->getbTO2CardDetected()))
                {
                    this->setMaquina(ESTADO_ESPERANDO_A_RETIRAR_TARJETA);
                    this->reclaim();
                    qDebug()<<"Se ha pasado timeout. Mostrar se ha tragado tarjeta por seguridad";
                    emit this->sgnTarjetaTragada(this->identificador_tarjeta);
                }

                if((this->mDispensador->getbTO2CardDetected()) && (!this->mDispensador->getbTO1CardDetected())&&(!this->mDispensador->getbTO3CardDetected()))
                {
                    this->mDispensador->terminate();
                    this->reclaim();
                    this->mDispensador->reclaim();
                    this->mDispensador->start();
                    this->resetDispensador();

                    this->setMaquina(ESTADO_ESPERANDO_A_RETIRAR_TARJETA);
                    qDebug()<<"Se ha pasado timeout. Mostrar se ha tragado tarjeta por seguridad";
                    emit this->sgnTarjetaTragada(this->identificador_tarjeta);
                }
               if((!this->mDispensador->getbTO2CardDetected()) && (!this->mDispensador->getbTO1CardDetected())&&(this->mDispensador->getbTO3CardDetected()))
                {
                    this->reclaim();
                    qDebug()<<"Se ha pasado timeout. Mostrar se ha tragado tarjeta por seguridad";
                    emit this->sgnTarjetaTragada(this->identificador_tarjeta);
                    this->setMaquina(ESTADO_ESPERANDO_A_RETIRAR_TARJETA);

                }
               else
               {
                   qDebug()<<"Ha recogido tarjeta pero no ha dado a continuar. Volver al inicio";
                   this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                   emit this->sgnRegresar();
               }
            }


            break;
        }
        case ESTADO_COMPROBANDO_TARJETA:
        {
            if((!this->mDispensador->getbTO2CardDetected()) &&(!this->mDispensador->getbTO3CardDetected()))
            {
                this->setMaquina(ESTADO_DISPENSADOR_REPOSO);
                qDebug()<<"Ha recogido la tarjeta. Mostrar la siguiente pagina";
                emit this->sgnSiguientePantalla();
            }
            else
            {
                this->semipoping();
                this->setMaquina(ESTADO_ESPERANDO_RECOGIDA_USUARIO);
                qDebug()<<"No ha recogido la tarjeta. Mostrar recoja su tarjeta y pulse continuar";
            }
            break;
        }

        }
    }
}
void Hilo::request()
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_REQUEST;
    this->mDispensador->agregarALaLista(temp);
}

void Hilo::anticol()
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_anticoll;
    this->mDispensador->agregarALaLista(temp);
}
void Hilo::seleccionar(QByteArray rfid)
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_seleccionar;
    temp.argumentos.append(rfid);

    this->mDispensador->agregarALaLista(temp);
}
void Hilo::agregar(int sector,int bloque)
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_agregar;
    temp.argumentos.append(sector);
    temp.argumentos.append(bloque);
    this->mDispensador->agregarALaLista(temp);
}
void Hilo::transferir()
{
   STR_MENSAJE temp;
   temp.comando=COMANDO_DISPENSADOR_transfer;
   this->mDispensador->agregarALaLista(temp);
}
void Hilo::leer(int bloque,int sector)
{
    this->ultimaLectura.clear();
    this->lecturaFinalizada=false;
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_irakurri;
    temp.argumentos.append(bloque);
    temp.argumentos.append(sector);

    this->mDispensador->agregarALaLista(temp);
}
void Hilo::verificar(QByteArray key,int bloque,int sector)
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_VERIFY;
    temp.argumentos.append(key);
    temp.argumentos.append(bloque);
    temp.argumentos.append(sector);

    this->mDispensador->agregarALaLista(temp);
}

void Hilo::cargarClave(QByteArray key)
{
    /*if(key.count()==6)
    {
        STR_MENSAJE temp;
        temp.comando=COMANDO_DISPENSADOR_loadkey;
        temp.argumentos.append(key);
        this->mDispensador->agregarALaLista(temp);
    }
    else
    { this->getStatus();
        qDebug()<<"Error al cargar clave. deben ser 6 bytes.";
    }*/
}
 void Hilo::autenticar(int bloque,int sector)
 {
     if( (bloque<16)&&(sector<4))
     {
         STR_MENSAJE temp;
         temp.comando=COMANDO_DISPENSADOR_authenticate;
         temp.argumentos.append(bloque);
         temp.argumentos.append(sector);
         this->mDispensador->agregarALaLista(temp);

     }
 }

void Hilo::decrementar(int bloque,int sector,int operacion)
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_decrease;
    temp.argumentos.append(sector);
    temp.argumentos.append(bloque);
    temp.argumentos.append(operacion);
    this->mDispensador->agregarALaLista(temp);
}
void Hilo::escribir(int bloque,int sector,QByteArray datos)
{

    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_idatzi;
    temp.argumentos.append(bloque);
    temp.argumentos.append(sector);
    if(datos.count()==16)
    {
        temp.argumentos.append(datos);
        this->mDispensador->agregarALaLista(temp);
    }
    else
    {
        qDebug()<<"Error";
    }
}
void Hilo::semidispensing()
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_semi_dispensing;
    this->mDispensador->agregarALaLista(temp);
}
void Hilo::semipoping()
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_semi_popping;
    this->mDispensador->agregarALaLista(temp);
}
void Hilo::fullout()
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_fullout;
    this->mDispensador->agregarALaLista(temp);
}
void Hilo::reclaim()
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_reclaim;
    this->mDispensador->agregarALaLista(temp);
}
void Hilo::getStatus()
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_getStatus;
    this->mDispensador->agregarALaLista(temp);
}
void Hilo::resetDispensador()
{
    STR_MENSAJE temp;
    temp.comando=COMANDO_DISPENSADOR_reset;
    this->mDispensador->agregarALaLista(temp);
}

WORD Hilo::CalcularCRC(QByteArray Buffer, BYTE len)
{
    WORD crc;	// Acumulador CRC
    BYTE i,j;	// Contadores

    crc = 0;
    for(j = 0; j < len; j++)
    {
        crc = crc ^ (Buffer[j] << 8);
        for(i = 0; i < 8; i++)
        {
            if ((crc & 0x8000) == 0x8000)
            {
                crc = crc << 1;
                crc ^= 0x1021;
            }
            else
            {
                crc = crc << 1;
            }
        }
    }
    return(crc);
}
