#include "salvapantallas.h"
#include "ui_salvapantallas.h"


Salvapantallas::Salvapantallas(QString path, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Salvapantallas)
{
    ui->setupUi(this);

    movie = new QMovie(path);
    mLab.init();


    connect(&mLab,SIGNAL(sgnClick(int,int)),this,SLOT(sltClick(int,int)));
    this->mLab.setScaledContents(true);
    this->mLab.setMovie(movie);

    ui->layoutVideo->addWidget(&mLab);

}

void Salvapantallas::sltClick(int,int)
{
    this->movie->stop();
    emit this->sgnClickEnSalvapantallas();
    this->close();
}
void Salvapantallas::empezar()
{
    this->showFullScreen();
    this->movie->start();
}

Salvapantallas::~Salvapantallas()
{
    delete ui;
}
