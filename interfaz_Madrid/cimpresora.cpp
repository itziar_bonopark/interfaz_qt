#include "cimpresora.h"

CImpresora::CImpresora(QObject *parent) :
    QObject(parent)
{
    //this->abrirPuerto();
}

void CImpresora::setRuta(QString ruta)
{
    this->ruta_logo=ruta;
}

void CImpresora::recepcion()
{
    int q = this->port->bytesAvailable();

    if(q>0)
    {
        qDebug()<<"He recibido la respuesta de la impresora!";
        QByteArray m= this->port->readAll();
        qDebug()<<m.toHex();
        this->estadoImpresora=m;
        QByteArray sta=intToBits(m[0]);
        sta.remove(0,3);
        if(quint8(sta[0])==0)
        {
            this->bhaypapel=true;
        }
        else this->bhaypapel=false;
        if(quint8(sta[1])==0)
        {
            this->bpapelpresente=true;
        }
        else this->bpapelpresente=false;

        if(quint8(sta[2])==0)
        {
            this->bpapelsuficiente=true;
            this->bPapelApuntoDeAcabar=false;
        }
        else
        {
            this->bpapelsuficiente=false;
            this->bPapelApuntoDeAcabar=true;
        }
        if(quint8(sta[3])==1)
        {
            this->bcutterOn=true;
        }
        else this->bcutterOn=false;

        if(quint8(sta[4])==1)
        {
            this->bencendido=true;
        }
        else this->bencendido=false;




        if(!this->bpapelsuficiente)
        {
            this->ok=false;
            qDebug()<<"No hay papel suficiente......";
        }

        else this->ok=true;

        emit this->estadoRecibido();
        emit this->recibidoDeLaImpresora(m);
    }
}
void CImpresora::imprimirLogo(QString ruta)
{


    IplImage* img = 0;
    int height,width,step,channels;


    // load an image
    QByteArray array = this->ruta_logo.toLocal8Bit();
    const char* imagen = array.data();
    img=cvLoadImage(imagen,0);

    if(!img){
        printf("Could not load image file: \n");
      exit(0);
    }

    cvFlip(img,img,1);


    // get the image data
    height    = img->height;
    width     = img->width;
    step      = img->widthStep;
    channels  = img->nChannels;
    uchar* dataBlack    = (uchar *)img->imageData;
    printf("Processing a %dx%d image with %d channels\n",height,width,channels);

    // create a window
    IplImage *bn = NULL, *canny = NULL;
    bn = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);
    canny = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, 1);


   QList<int> imagenDOT;

   QList<bool> bitsdots;
   int v=int(bn->height)/8;
   int hneed=8*(v+1);
   IplImage* newimage=cvCreateImage(cvSize(bn->width, hneed),bn->depth,1);
   uchar* datanewimage=(uchar*)newimage->imageData;
   for(int i=0;i<newimage->height;i++)
   {
       for(int j=0;j<newimage->width;j++)
       {
           datanewimage[i*newimage->widthStep+j]=255;
       }
   }

   for(int i=0;i<bn->height;i++)
   {
       for(int j=0;j<bn->width;j++)
       {
           int value=0;

           if(dataBlack[i*bn->widthStep+j]>100)
           {
               value=255;

           }

           datanewimage[i*newimage->widthStep+j]=value;
       }
   }




   for(int m=0;m<v;m++)
   {
       for(int j=0;j<newimage->width;j++)
       {
           for(int i=0;i<8;i++)
           {
               bool bit=true;
               if(datanewimage[(i+8*m)*newimage->widthStep+j]>100)
               {
                   bit=false;
               }
               bitsdots.append(bit);
               if(bitsdots.count()==8)
               {
                   imagenDOT.prepend(bitToInt(bitsdots));
                   bitsdots.clear();
               }

           }
       }
       this->selectBitImageMode(0,L(imagenDOT.count()),H(imagenDOT.count()),imagenDOT);
        imagenDOT.clear();
   }

    // release the image
    cvReleaseImage(&img );


}

void CImpresora::abrirPuerto()
{
    qDebug() << "Abriendo comunicaciones con la impresora...";

    this->port = new QextSerialPort(this->puerto_impresora, QextSerialPort::EventDriven);
    port->setBaudRate(BAUD115200);
    port->setFlowControl(FLOW_OFF);
    port->setParity(PAR_NONE);
    port->setDataBits(DATA_8);
    port->setStopBits(STOP_1);

    if (port->open(QIODevice::ReadWrite) == true) {
        connect(port, SIGNAL(readyRead()), this, SLOT(recepcion()));

        if (!(port->lineStatus() & LS_DSR))
            qDebug() << "warning: device is not turned on";
        qDebug() << "listening for data on" << port->portName();
    }
    else {
        qDebug() << "device failed to open:" << port->errorString();
    }
}

void CImpresora::imprimirTexto(QString texto,bool fin,int salto)
{
    QByteArray mensaje;
    mensaje.append(0x0a);
    this->port->write(mensaje);
    emit this->enviadoALaImpresora(mensaje);

    mensaje.clear();
    mensaje.append(texto);
    this->port->write(mensaje);
    emit this->enviadoALaImpresora(mensaje);


    mensaje.clear();
    mensaje.append(0x1b);
    mensaje.append(0x64);
    mensaje.append(salto);
    this->port->write(mensaje);
    emit this->enviadoALaImpresora(mensaje);


    mensaje.clear();
    if(fin)
    {
        mensaje.append(0x1d);
        mensaje.append(0x56);
        mensaje.append(1);
        mensaje.append(1);
        this->port->write(mensaje);
        emit this->enviadoALaImpresora(mensaje);

    }
    else
    {

    }
}

void CImpresora::setJustificacion(int jus)
{
    QByteArray mensaje;
    mensaje.append(0x1b);
    mensaje.append(0x61);
    mensaje.append(jus);
    this->port->write(mensaje);
    emit this->enviadoALaImpresora(mensaje);

}
void CImpresora::setCharacterSize(int siz)
{
    QByteArray mensaje;
    mensaje.append(0x1d);
    mensaje.append(0x21);
    mensaje.append(siz);
    this->port->write(mensaje);
    emit this->enviadoALaImpresora(mensaje);

}

void CImpresora::setCharacterFont(int v)
{
    QByteArray mensaje;
    mensaje.append(0x1b);
    mensaje.append(0x4d);

    mensaje.append(v);
    this->port->write(mensaje);
    emit this->enviadoALaImpresora(mensaje);

}
void CImpresora::selectBitImageMode(int m, int nL, int nH,QList<int> d)
{
    QByteArray mensaje;
    mensaje.append(0x1b);
    mensaje.append(0x2a);

    mensaje.append(m);
    mensaje.append(nL);
    mensaje.append(nH);
    for(int i=0;i<d.count();i++)mensaje.append(d[i]);
    this->port->write(mensaje);
    emit this->enviadoALaImpresora(mensaje);

}
void CImpresora::getEstado()
{
    QByteArray mensaje;
    mensaje.append(0x1c);
    mensaje.append(0x76);
    this->port->write(mensaje);
    this->iLastCommand=COMANDO_ESTADO;
    emit this->enviadoALaImpresora(mensaje);

}
