#ifndef HILO_H
#define HILO_H

#include <QThread>
#include"neodispensador.h"


#define WORD unsigned int
#define BYTE unsigned char
#define L(v) ((BYTE) (v))
#define H(v)  ((BYTE) (((WORD) (v)) >> 8))

struct STR_INFO
{
    quint8 cod;
    quint8 d1;
    quint8 xx1;
    quint8 xx2;
    quint8 d2;
    quint8 xx3;
    quint8 xx4;
    quint8 xx5;

    quint8 d3;
    quint8 xx6;
    quint8 xx7;
    quint8 d4;
    quint8 xx8;
    quint8 d5;
    quint8 xx9;
    quint8 d6;

};
union UNI_INFO
{
    STR_INFO estructura;
    quint8 datos[sizeof(estructura)];
};


class Hilo : public QThread
{
    Q_OBJECT
public:
    explicit Hilo(QObject *parent = 0);
    neoDispensador* mDispensador;

    WORD CalcularCRC(QByteArray Buffer, BYTE len);

    void run();
    void request();
    void anticol();
    void seleccionar(QByteArray rfid);
    void verificar(QByteArray key, int bloque, int sector);
    void agregar(int sector,int bloque);
    void transferir();
    void leer(int bloque,int sector);
    void cargarClave(QByteArray key);
    void autenticar(int bloque,int sector);
    void decrementar(int bloque,int sector,int operacion);
    void escribir(int bloque,int sector,QByteArray datos);
    void semidispensing();
    void semipoping();
    void fullout();
    void reclaim();
    void getStatus();
    void resetDispensador();

    void setMaquina(int m);
    int getMaquina();
    void setTiempoRecogida(int m);
    void setTiempoIntroduccion(int m);

    QString puerto_dispensador;

    
signals:
    void sgnRegresar();
    void sgnTarjetaLeida(QByteArray rfid);
    void sgnDispensarTarjeta();
    void sgnLeyendoTarjeta();
    void sgnErrorLecturaTarjeta();
    void sgnSiguientePantalla();
    void sgnTarjetaTragada(QByteArray rfid);
    void sgnMostrarMenu();

public slots:
    void sltLecturaFinalizadaDeBloqueYsector(QByteArray msg);
private:
    UNI_INFO mInfo;
    int maq;
    bool lecturaFinalizada;
    QByteArray ultimaLectura;
    bool continuar;
    QDateTime fechaCambio;
    QDateTime fechaCambioEstado;
    QByteArray identificador_tarjeta;
    int fallos_lectura;
    int tiempo_espera_recogida_tarjeta;
    int tiempo_espera_introduccion_tarjeta;

};

#endif // HILO_H
