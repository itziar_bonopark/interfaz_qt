#include <QtGui/QApplication>
#include "mainwindow.h"
#include <QTextCodec>
#include<QDebug>
#include<QFile>
#include<QDir>
#include<QtGlobal>
#include <syslog.h>


void myMessageOutput(QtMsgType type, const char *msg)
{
    //in this function, you can write the message to any stream!
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s\n", msg);
        syslog (LOG_NOTICE, "%s",msg);

        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s\n", msg);
        syslog (LOG_WARNING,"%s", msg);

        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s\n", msg);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s\n", msg);
        abort();
    }

}

int main(int argc, char *argv[])
{
    setlogmask (LOG_UPTO (LOG_NOTICE|LOG_WARNING|LOG_CRIT));
    openlog ("interfaz totem", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
    qInstallMsgHandler(myMessageOutput); //install : set the callback



    QApplication a(argc, argv);
    MainWindow w;
    //w.show();
   w.showFullScreen();

    QTextCodec *linuxCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(linuxCodec);
    QTextCodec::setCodecForCStrings(linuxCodec);
    QTextCodec::setCodecForLocale(linuxCodec);
    
    return a.exec();
}
