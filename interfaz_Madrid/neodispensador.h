#ifndef NEODISPENSADOR_H
#define NEODISPENSADOR_H

#include <iostream>
#include<QDebug>
#include<QDateTime>
#include<QTimer>

#include <QThread>
#include"comandos_dispensador.h"
#include"estadosDispensador.h"
#include<SerialStream.h>
#include<QEventLoop>


#define WORD unsigned int
#define BYTE unsigned char
#define L(v) ((BYTE) (v))
#define H(v)  ((BYTE) (((WORD) (v)) >> 8))

using namespace LibSerial;

struct STR_MENSAJE
{
    int comando;
    QByteArray argumentos;
};

class neoDispensador : public QThread
{
    Q_OBJECT
public:
    explicit neoDispensador(QObject *parent = 0);
    QByteArray prepararMensajeEnCasoDeDiferente1(QByteArray msg);
    QByteArray escribeYespera(QByteArray msg,int ms=200,bool diferente=false);
    void inicializacion();
    void run();
    void abrirPuerto();
    void agregarALaLista(STR_MENSAJE msg);
    quint8 getXor(QByteArray mensaje,int len);
    quint8 checksum(QByteArray mensaje);

    QByteArray intToBits(quint8 entero);
    void setEstado(int est);
    QByteArray getRfid();
    void getStatus();
    void request();
    void anticoll();

    void seleccionar(QByteArray rfid);
    void agregar(int sector, int bloque, int operacion);
    void transfer();
    QByteArray leer(int bloque, int sector);
    void verify(QByteArray codigo,int bloque,int sector);
    void loadkey(QByteArray codigo);
    void authenticate(int sector, int bloque);
    void decrease(int bloque,int sector, int operacion);
    void escribe(int sector, int bloque, QByteArray datos);
    void semi_dispensing(); //pasa a la posicion de lectura de tarjeta
    void semi_popping();// dispensa la tarjeta
    void fullout();
    void reclaim();
    void resetDispensador();
    void semipoping();
    void semidispensing();
    void resetRFID();



    bool getTarjetas_Dispensador(){return tarjetas_dispensador;}
    void setTarjetas_Dipensador(bool tarjetas){tarjetas_dispensador=tarjetas;}

    bool getbTO3CardDetected ();// devolver[11]();
    bool getbTO2CardDetected ();// devolver[10]();
    bool getbTO1CardDetected ();// devolver[9]();
    bool getbStackHolderEmpty ();// devolver[8]();
    bool getbHolderPreEmpty ();// devolver[7]();
    bool getbCardJamming ();// devolver[6]();
    bool getbROCardDetected ();// devolver[5]();
    bool getbReservation ();// devolver[4]();
    bool getbReclaimingError ();// devolver[3]();
    bool getbDispensingError ();// devolver[2]();
    bool getbReclaiming ();// devolver[1]();
    bool getbDispensing ();// devolver[0]();

    int getEstado();

    QByteArray prepararMensajeEnCasoDeDiferente(QByteArray msg);
    void esperar(int ms);
    void limpiarLista();
    bool getEstadoDefinido();
    void setEstadoDefinido(bool v);

    QString puerto;

signals:
    void sgnLecturaFinalizadaDelBloqueYsector(QByteArray msg);
    
public slots:

private:
    QByteArray respuesta;
    bool posible;
   int fallosConsecutivos;
    SerialStream serial_port;
    int estado;
    QList<STR_MENSAJE> listaComandos;

    bool bTO3CardDetected ;// devolver[11];
    bool bTO2CardDetected ;// devolver[10];
    bool bTO1CardDetected ;// devolver[9];
    bool bStackHolderEmpty ;// devolver[8];
    bool bHolderPreEmpty ;// devolver[7];
    bool bCardJamming ;// devolver[6];
    bool bROCardDetected ;// devolver[5];
    bool bReservation ;// devolver[4];
    bool bReclaimingError ;// devolver[3];
    bool bDispensingError ;// devolver[2];
    bool bReclaiming ;// devolver[1];
    bool bDispensing ;// devolver[0];

    int numBytesEsperados;

    QByteArray bufferRecepcion;
    bool esperandoRespuesta;
    QByteArray rfid;
    int intentoLectura;
    bool esperandoLeerTarjeta;
    neoDispensador*mnDispensador;

    bool tarjetas_dispensador;
    QByteArray idrfid;

    bool estadoDefinido;
};

#endif // NEODISPENSADOR_H
