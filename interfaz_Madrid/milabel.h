#ifndef MILABEL_H
#define MILABEL_H
#include<QDebug>
#include <QLabel>
#include<QMouseEvent>
class milabel : public QLabel
{
    Q_OBJECT
public:
    explicit milabel(QWidget *parent = 0);
    int xInicial;
    int yInicial;
    int deltaX;
    int deltaY;


    QPoint m_prev;
    QPoint diff;
    QPoint prev_diff;
    int n;


signals:
    void sgnMovimiento(int x,int y);
    void sgnClick(int x,int y);
    void sgnPosicionaScrolls(int x,int y);

public slots:
    void init();
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);

};

#endif // MILABEL_H
