
#include <qwebview.h>
#include <qwebframe.h>
#include <QDebug>
#include "testobject.h"


MyApi::MyApi( QObject *parent )
    : QObject( parent )
{

}
void MyApi::setIdEstacion(int id)
{
    this->idestacion=id;
}

void MyApi::setWebView( QWebView *view )
{
    QWebPage *page = view->page();
    frame = page->mainFrame();

    attachObject();
    connect( frame, SIGNAL(javaScriptWindowObjectCleared()), this, SLOT(attachObject()) );
}


void MyApi::attachObject()
{
    frame->addToJavaScriptWindowObject( QString("MyApi"), this );
}

void MyApi::click()
{
    emit this->sgnClick();
}

bool MyApi::getPapel()
{
    return this->papel;
}
bool MyApi::getTarjetas()
{
    return this->tarjetas;
}
void MyApi::setPapel(bool id)
{
    this->papel=id;
}
void MyApi::setTarjetas(bool id)
{
    this->tarjetas=id;
}

void MyApi::esperandoRecogidaAbono()
{
   this->sgnRecogidaAbono();
}
void MyApi::obtenerTarjeta()
{
    emit this->sgnObtenerTarjeta();
}
void MyApi::VentanaAverias()
{
    emit this->sgnVentanaAverias();
}
void MyApi::getID()
{
    qDebug()<<"me pide la estacion";
    emit this->sgnIdEstacion();
}

void MyApi::imprimirTrayecto(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString rfid=lista[0];
    QString idestacion_desenganche=lista[1];
    QString idestacion_enganche=lista[2];
    QString importe=lista[3];
    QString tiempo_trayecto=lista[4];
    QString fecha_desenganche=lista[5];
    QString fecha_enganche=lista[6];
    QString dni=lista[7];
    emit this->sgnImprimirTrayecto(rfid,idestacion_desenganche,idestacion_enganche,importe,tiempo_trayecto,fecha_desenganche,fecha_enganche,dni);
}


void MyApi::ExpulsarTarjeta()
{
    emit this->sgnExpulsarTarjeta();
}

void MyApi::ReclamarTarjeta()
{
    emit this->sgnReclamarTarjeta();
}

void MyApi::mandarcorreo(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString mail=lista[0];
    QString tipo=lista[1];
    QString fecha_caducidad=lista[2];
    QString id_usuario=lista[3];
    QString nombre=lista[4];
    QString apellido1=lista[5];
    QString apellido2=lista[6];
    QString password=lista[7];
    QString telefono=lista[8];

    emit this->sgnmandarcorreo(mail,tipo,fecha_caducidad,id_usuario,nombre,apellido1,apellido2,password,telefono);
}


void MyApi::imprimirCompra(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString rfid=lista[0];
    QString dni=lista[1];
    QString importe_abono=lista[2];
    QString saldo_inicial=lista[3];
    emit this->sgnImprimirCompra(rfid,dni,importe_abono,saldo_inicial);
}

void MyApi::imprimirRecarga(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString rfid=lista[0];
    QString dni=lista[1];
    QString recarga=lista[2];
    emit this->sgnImprimirRecarga(rfid,dni,recarga);
}

void MyApi::imprimirTurista(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString rfid=lista[0];
    QString dni=lista[1];
    QString duracion=lista[2];
    QString importe=lista[3];
    emit this->sgnImprimirTurista(rfid,dni,duracion,importe);
}

void MyApi::crearAbonoRecogida(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString dni=lista[0];
    QString tipo=lista[1];
    QString estado=lista[2];
    QString tipo_documento=lista[3];
    QString idrfid=lista[4];
    QString duracion=lista[5];
    QString saldo=lista[6];
    emit this->sgncrearAbonoRecogida(dni,tipo,estado,tipo_documento,idrfid,duracion,saldo);
}

void MyApi::continuar()
{
    emit this->sgncontinuar();
}

void MyApi::compraAbono(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString importe=lista[0];
    QString tipo_transaccion=lista[1];
    emit this->sgncompraAbono(importe,tipo_transaccion);
}

void MyApi::devolucionPreautorizacion(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString pedido=lista[0];
    QString rts=lista[1];
    emit this->sgndevolucionPreautorizacion(pedido,rts);
}

void MyApi::imprimirTuristaDenegada()
{
    emit this->sgnImprimirTuristaDenegada();
}

void MyApi::imprimirAnulacion()
{
    emit this->sgnImprimirAnulacion();
}

void MyApi::recargaAbono(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString importe=lista[0];
    QString tipo_transaccion=lista[1];
    emit this->sgnrecargaAbono(importe,tipo_transaccion);
}

void MyApi::devolucionRecarga(QString datos)
{
    QList<QString> lista=datos.split("#");
    QString pedido=lista[0];
    QString rts=lista[1];
    QString importe=lista[2];
    emit this->sgndevolucionRecarga(pedido,rts,importe);
}

void MyApi::imprimirRecargaDenegada()
{
    emit this->sgnImprimirRecargaDenegada();
}

void MyApi::imprimirDevolucion()
{
    emit this->sgnImprimirDevolucion();
}
