#ifndef COMANDOS_DISPENSADOR_H
#define COMANDOS_DISPENSADOR_H

const int COMANDO_DISPENSADOR_REQUEST                               = 0;
const int COMANDO_DISPENSADOR_anticoll                              = 1 ;//);
const int COMANDO_DISPENSADOR_seleccionar                           = 2 ;//);
const int COMANDO_DISPENSADOR_agregar                               = 3 ;//int sector, int bloque, int operacion);
const int COMANDO_DISPENSADOR_transfer                              = 4 ;//);
const int COMANDO_DISPENSADOR_irakurri                              = 5 ;//int bloque, int sector);
const int COMANDO_DISPENSADOR_loadkey                               = 6 ;//QList<QString> codigo);
const int COMANDO_DISPENSADOR_authenticate                          = 7 ;//);
const int COMANDO_DISPENSADOR_decrease                              = 8 ;//int bloque,int sector, int operacion);
const int COMANDO_DISPENSADOR_idatzi                                = 9 ;//int sector, int bloque, QList<QString> datos);
const int COMANDO_DISPENSADOR_semi_dispensing                       = 10 ;//); //pasa a la posicion de lectura de tarjeta
const int COMANDO_DISPENSADOR_semi_popping                          = 11 ;//);// dispensa la tarjeta
const int COMANDO_DISPENSADOR_fullout                               = 12 ;//);
const int COMANDO_DISPENSADOR_reclaim                               = 13 ;//);
const int COMANDO_DISPENSADOR_getStatus                             = 14 ;//);
const int COMANDO_DISPENSADOR_comprobarSiHayTarjetaSinHaberlaPedido = 15 ;//);
const int COMANDO_DISPENSADOR_setRF                                 = 16 ;//bool on);

const int COMANDO_DISPENSADOR_reset                                 = 17 ;//);
const int COMANDO_DISPENSADOR_setBuzzer                             = 18 ;//int u);

const int COMANDO_DISPENSADOR_VERIFY                                = 19;


#endif // COMANDOS_DISPENSADOR_H
