#-------------------------------------------------
#
# Project created by QtCreator 2014-03-26T16:01:52
#
#-------------------------------------------------

QT       += core gui webkit webkitwidgets widgets sql network xml

TARGET = interfaz_Madrid
TEMPLATE = app

DEPENDPATH += .
INCLUDEPATH += /home/rich/src/qtwebkitextension/. .



SOURCES += main.cpp\
        mainwindow.cpp \
    salvapantallas.cpp \
    cimpresora.cpp \
    testobject.cpp \
    milabel.cpp \
    commonFunctions.cpp \
    smtpclient.cpp \
    quotedprintable.cpp \
    mimetext.cpp \
    mimepart.cpp \
    mimemultipart.cpp \
    mimemessage.cpp \
    mimeinlinefile.cpp \
    mimehtml.cpp \
    mimecontentformatter.cpp \
    mimeattachment.cpp \
    emailaddress.cpp \
    mimefile.cpp \
    neodispensador.cpp \
    hilo.cpp

HEADERS  += mainwindow.h \
    salvapantallas.h \
    comandos_dispensador.h \
    cimpresora.h \
    testobject.h \
    milabel.h \
    commonFunctions.h \
    smtpclient.h \
    quotedprintable.h \
    mimetext.h \
    mimepart.h \
    mimemultipart.h \
    mimemessage.h \
    mimeinlinefile.h \
    mimehtml.h \
    mimefile.h \
    mimecontentformatter.h \
    mimeattachment.h \
    emailaddress.h \
    SmtpMime \
    neodispensador.h \
    hilo.h \
    estadosDispensador.h \
    TPVPC.h

FORMS    += mainwindow.ui \
    salvapantallas.ui

LIBS+= -L/usr/local/lib -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_flann
LIBS+=-lserial
CONFIG += extserialport
