#include "milabel.h"

milabel::milabel(QWidget *parent) :
    QLabel(parent)
{
    this->n=1;
    this->prev_diff.setX(0);
    this->prev_diff.setY(0);
}

void milabel::init()
{
    this->n=1;
    this->prev_diff.setX(0);
    this->prev_diff.setY(0);
}

void milabel::mouseMoveEvent(QMouseEvent *ev)
{
    QPoint pt = ((QMouseEvent*)ev)->pos();
    diff = -(pt-m_prev-prev_diff*n);
    m_prev = pt;
    emit this->sgnPosicionaScrolls(diff.x()*n,diff.y()*n);
    this->prev_diff=diff;


}
void milabel::mousePressEvent(QMouseEvent *ev)
{
    m_prev = ((QMouseEvent*)ev)->pos();
    this->prev_diff.setX(0);
    this->prev_diff.setY(0);
    emit this->sgnClick(ev->x(),ev->y());
}

void milabel::mouseReleaseEvent(QMouseEvent *ev)
{

}
