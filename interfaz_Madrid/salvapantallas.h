#ifndef SALVAPANTALLAS_H
#define SALVAPANTALLAS_H

#include <QDialog>
#include "milabel.h"
#include <QMovie>
namespace Ui {
class Salvapantallas;
}

class Salvapantallas : public QDialog
{
    Q_OBJECT
    
public:
    explicit Salvapantallas(QString path,QWidget *parent = 0);
    ~Salvapantallas();
    void empezar();

public slots:
    void sltClick(int, int);

signals:
    void sgnClickEnSalvapantallas();
    
private:
    Ui::Salvapantallas *ui;
    milabel mLab;
    QMovie *movie;
};

#endif // SALVAPANTALLAS_H
