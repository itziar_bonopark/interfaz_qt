#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <testobject.h>
#include <salvapantallas.h>
#include <QTimer>
#include <cimpresora.h>
#include <QWebFrame>
#include <QEventLoop>
#include <QTimer>
#include <hilo.h>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QProcess>
#include"SmtpMime"
#include <QDesktopServices>
#include <QSettings>
#include <QFile>
#include <QtXml>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool ComprobarTarjetas();
    bool ComprobarPapel();

public slots:
    void sendMail(QString to="", QString subject="", QString body="");
    void sltRegresar();

private slots:
    void sltTimeoutSalvapantallas();
    void sltClickEnSalvapantallas();
    void resetTimer();
    void sltActividad();

    void sltIntencionDeRecogerAbono();
    void sltRecogidaAbonoMal();
    void obtenerConfiguracion();
    void sltTarjetaTragada(QByteArray idrfid);
    void sltTarjetaLeida(QByteArray identificacion_tarjeta);
    void sltObtenerTarjeta();
    void sltVentanaAverias();
    void sltGetIdEstacion();
    void sltImprimirTrayecto(QString rfid,QString idestacion_desenganche,QString idestacion_enganche,QString importe,QString tiempo_trayecto,QString fecha_desenganche,QString fecha_enganche,QString dni);
    void sltExpulsarTarjeta();
    void sltReclamarTarjeta();
    void sltmandarcorreo(QString mail,QString tipo,QString fecha_caducidad,QString id_usuario,QString nombre,QString apellido1,QString apellido2,QString password,QString telefono);
    void sltImprimirCompra(QString rfid, QString dni, QString importe_abono, QString saldo_inicial);
    void sltImprimirRecarga(QString rfid, QString dni, QString recarga);
    void sltImprimirTurista(QString rfid,QString dni,QString duracion,QString importe);
    void sltTimeoutComprobaciones();
    void sltcrearAbonoRecogida(QString dni,QString tipo,QString estado,QString tipo_documento,QString idrfid,QString duracion,QString saldo);
    void sltLeyendoTarjeta();
    void sltErrorLecturaTarjeta();
    void sltcontinuar();
    void sltSiguientePantalla();

    void sltcompraAbono(QString importe,QString tipo_transaccion);
    void sltdevolucionPreautorizacion(QString pedido,QString rts);
    void sltImprimirTuristaDenegada();
    void sltImprimirAnulacion();

    void sltrecargaAbono(QString importe,QString tipo_transaccion);
    void sltdevolucionRecarga(QString pedido,QString rts,QString importe);
    void sltImprimirRecargaDenegada();
    void sltImprimirDevolucion();

    void sltMostrarMenu();


private:
    Ui::MainWindow *ui;
    MyApi *myapi;
    QTimer* timerSalvapantallas;
    QTimer* timer_tarjeta;
    Salvapantallas* s;
    Hilo* mHilo;
    CImpresora* mImpresora;

    bool tarjetas_dispensador;
    bool papel_impresora;

    int idestacion;
    QString mIP;
    QSqlDatabase database;
    QProcess* teclado;
    bool hayTeclado;
    QString web_interfaz;
    QSettings* mSettings;
    QDomDocument xmlBOM;
    QTcpSocket* sktPagos;
    QString path_salvapantallas;
    QString url_menu;

};

#endif // MAINWINDOW_H
